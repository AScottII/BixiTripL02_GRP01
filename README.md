# BixiTrip

Team Name: Coding Queens

Team Members: Harsh Mahajan, Cassidy Baldin, Alan Scott

This project is a reimplementation of BixiTrip

The folders and files for this project are as follows:

Doc - Documentation for the project

src - The code

Installers - different installer files for all stable builds of the program, BixiTripInstaller is the most recent

## System Requirements
- Windows 10
- Java 17 installed

## Installation
1. Download BixiTrip.msi (or the other installers for past versions)
2. Run the installer file
3. Complete the installation wizard

## Uninstalling
1. Run the same installer file
2. Select the uninstall option and confirm

## Installer Versions
- BixiTrip.msi - Full version as of final demonstration
- ProofOfConcept.msi - Proof of concept installer file
- Revision0.msi - Revision 0 demonstration installer file
