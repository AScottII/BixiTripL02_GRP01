var namespacealgs =
[
    [ "BinarySearch", "classalgs_1_1_binary_search.html", null ],
    [ "Graph", "classalgs_1_1_graph.html", "classalgs_1_1_graph" ],
    [ "IndexMinPQ", "classalgs_1_1_index_min_p_q.html", "classalgs_1_1_index_min_p_q" ],
    [ "PastTripsBUMergeSort", "classalgs_1_1_past_trips_b_u_merge_sort.html", null ],
    [ "Queue", "classalgs_1_1_queue.html", "classalgs_1_1_queue" ],
    [ "SP", "classalgs_1_1_s_p.html", "classalgs_1_1_s_p" ],
    [ "Stack", "classalgs_1_1_stack.html", "classalgs_1_1_stack" ],
    [ "StationsMergesort", "classalgs_1_1_stations_mergesort.html", null ]
];