var classbixi_trip_1_1_past_trips =
[
    [ "addTrip", "classbixi_trip_1_1_past_trips.html#a90b5e8146fc60c59a01b8ff2f994f215", null ],
    [ "getNextPath", "classbixi_trip_1_1_past_trips.html#a14e981a400e52b0523759738a3c4e360", null ],
    [ "initialize", "classbixi_trip_1_1_past_trips.html#a40b8e0f788fcd55066594e986377dc0a", null ],
    [ "sortPastTrips", "classbixi_trip_1_1_past_trips.html#ad43b4892b0b0c296346651550c211793", null ],
    [ "i", "classbixi_trip_1_1_past_trips.html#aed99f95a7abe24cf5aae0f78140738cb", null ],
    [ "isSorted", "classbixi_trip_1_1_past_trips.html#aaeb6e17700a6a64aacff83fd70b3af88", null ],
    [ "pastTrips", "classbixi_trip_1_1_past_trips.html#a4f2422008cec22f023d663ac751d03c7", null ]
];