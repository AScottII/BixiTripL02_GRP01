var searchData=
[
  ['smalltextfont16_363',['smallTextFont16',['../classbixi_trip_1_1_bixi_trip_u_i.html#a6832b76e076b12e8c647c9bc8bbe1193',1,'bixiTrip::BixiTripUI']]],
  ['sp_364',['sp',['../classbixi_trip_1_1_trip.html#af2b8f81e869d5e929b0e4dee842d1613',1,'bixiTrip::Trip']]],
  ['start_365',['start',['../classbixi_trip_1_1_trip.html#adb0b89b1a0436099db1699d89b01952c',1,'bixiTrip::Trip']]],
  ['startcode_366',['startCode',['../classbixi_trip_1_1_past_trip.html#a17b002c1ce3fddb33732a1fa92e37353',1,'bixiTrip::PastTrip']]],
  ['startstationcombobox_367',['startStationComboBox',['../classbixi_trip_1_1_bixi_trip_u_i.html#a482085d9e978008e6adf2986a1c37e4c',1,'bixiTrip::BixiTripUI']]],
  ['stations_368',['stations',['../classbixi_trip_1_1_path.html#ad2f674a61366f4c64eccb8a1713ede5a',1,'bixiTrip.Path.stations()'],['../classbixi_trip_1_1_paths.html#adf073d3e9badf4b172a0f50105875e2e',1,'bixiTrip.Paths.stations()'],['../classbixi_trip_1_1_stations.html#a032e96d455ed24835a1ca7af45457058',1,'bixiTrip.Stations.stations()'],['../classbixi_trip_1_1_trip.html#a29a6348d81c6f47139dfcb6a66911117',1,'bixiTrip.Trip.stations()']]],
  ['stationslist_369',['stationsList',['../classbixi_trip_1_1_bixi_trip_u_i.html#aeff23e50bc29089f8578e94f23704928',1,'bixiTrip::BixiTripUI']]],
  ['statusfield_370',['statusField',['../classbixi_trip_1_1_bixi_trip_u_i.html#a8ede5de69392d10fa3fd765c9cfad4f2',1,'bixiTrip::BixiTripUI']]],
  ['swapbutton_371',['swapButton',['../classbixi_trip_1_1_bixi_trip_u_i.html#a39e034210cdf23a7d08709988d9c508c',1,'bixiTrip::BixiTripUI']]]
];
