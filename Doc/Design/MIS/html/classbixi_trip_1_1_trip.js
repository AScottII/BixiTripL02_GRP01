var classbixi_trip_1_1_trip =
[
    [ "Trip", "classbixi_trip_1_1_trip.html#a18b4510b63e455cfb0a1b55b85f84118", null ],
    [ "getDuration", "classbixi_trip_1_1_trip.html#a1e8317cf394a78f7113f131c1b037de6", null ],
    [ "getRoute", "classbixi_trip_1_1_trip.html#a95b416d8bdc76abe46d72c1eec410bb4", null ],
    [ "getUrl", "classbixi_trip_1_1_trip.html#a9a3ba7c01f2afd56be43d6c1ce4679d3", null ],
    [ "end", "classbixi_trip_1_1_trip.html#a1f90cad16ad406d2c34ec6221321c696", null ],
    [ "route", "classbixi_trip_1_1_trip.html#a6c72e717ebdf4518951a5393a79aed04", null ],
    [ "sp", "classbixi_trip_1_1_trip.html#af2b8f81e869d5e929b0e4dee842d1613", null ],
    [ "start", "classbixi_trip_1_1_trip.html#adb0b89b1a0436099db1699d89b01952c", null ],
    [ "stations", "classbixi_trip_1_1_trip.html#a29a6348d81c6f47139dfcb6a66911117", null ]
];