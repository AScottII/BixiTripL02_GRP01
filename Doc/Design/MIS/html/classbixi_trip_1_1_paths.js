var classbixi_trip_1_1_paths =
[
    [ "Paths", "classbixi_trip_1_1_paths.html#ab3524c84c92b0793d785b7e79ee5618d", null ],
    [ "addPath", "classbixi_trip_1_1_paths.html#aafd33bbe2d65a520ab4c4d041a291886", null ],
    [ "getGraph", "classbixi_trip_1_1_paths.html#a9361be0a80cb167432c3e8b39b425f56", null ],
    [ "getPath", "classbixi_trip_1_1_paths.html#ac27bb3c256d9afe93ac862520c86f15c", null ],
    [ "importPastTrips", "classbixi_trip_1_1_paths.html#a4192647388f5993a8c764443617fdd77", null ],
    [ "graph", "classbixi_trip_1_1_paths.html#a841e63de2d855b0509df61873f300149", null ],
    [ "stations", "classbixi_trip_1_1_paths.html#adf073d3e9badf4b172a0f50105875e2e", null ]
];