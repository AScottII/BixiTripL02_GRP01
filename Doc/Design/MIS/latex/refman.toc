\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Namespace Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Packages}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}Hierarchical Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Class Hierarchy}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}%
\contentsline {chapter}{\numberline {4}File Index}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}File List}{7}{section.4.1}%
\contentsline {chapter}{\numberline {5}Namespace Documentation}{9}{chapter.5}%
\contentsline {section}{\numberline {5.1}Package algs}{9}{section.5.1}%
\contentsline {section}{\numberline {5.2}Package bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip}{9}{section.5.2}%
\contentsline {chapter}{\numberline {6}Class Documentation}{11}{chapter.6}%
\contentsline {section}{\numberline {6.1}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Binary\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Search Class Reference}{11}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Member Function Documentation}{11}{subsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.1.1}pathSearch()}{11}{subsubsection.6.1.1.1}%
\contentsline {subsubsection}{\numberline {6.1.1.2}stationSearch()}{12}{subsubsection.6.1.1.2}%
\contentsline {section}{\numberline {6.2}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}UI Class Reference}{12}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Constructor \& Destructor Documentation}{13}{subsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.1.1}BixiTripUI()}{13}{subsubsection.6.2.1.1}%
\contentsline {subsection}{\numberline {6.2.2}Member Function Documentation}{13}{subsection.6.2.2}%
\contentsline {subsubsection}{\numberline {6.2.2.1}main()}{13}{subsubsection.6.2.2.1}%
\contentsline {subsubsection}{\numberline {6.2.2.2}parse()}{14}{subsubsection.6.2.2.2}%
\contentsline {subsubsection}{\numberline {6.2.2.3}populate()}{14}{subsubsection.6.2.2.3}%
\contentsline {subsection}{\numberline {6.2.3}Member Data Documentation}{14}{subsection.6.2.3}%
\contentsline {subsubsection}{\numberline {6.2.3.1}bixiRed}{14}{subsubsection.6.2.3.1}%
\contentsline {subsubsection}{\numberline {6.2.3.2}contentPane}{14}{subsubsection.6.2.3.2}%
\contentsline {subsubsection}{\numberline {6.2.3.3}directionsButton}{14}{subsubsection.6.2.3.3}%
\contentsline {subsubsection}{\numberline {6.2.3.4}endStationComboBox}{15}{subsubsection.6.2.3.4}%
\contentsline {subsubsection}{\numberline {6.2.3.5}inspectButton}{15}{subsubsection.6.2.3.5}%
\contentsline {subsubsection}{\numberline {6.2.3.6}labelFont}{15}{subsubsection.6.2.3.6}%
\contentsline {subsubsection}{\numberline {6.2.3.7}progressBar}{15}{subsubsection.6.2.3.7}%
\contentsline {subsubsection}{\numberline {6.2.3.8}smallTextFont16}{15}{subsubsection.6.2.3.8}%
\contentsline {subsubsection}{\numberline {6.2.3.9}startStationComboBox}{15}{subsubsection.6.2.3.9}%
\contentsline {subsubsection}{\numberline {6.2.3.10}stationsList}{15}{subsubsection.6.2.3.10}%
\contentsline {subsubsection}{\numberline {6.2.3.11}statusField}{15}{subsubsection.6.2.3.11}%
\contentsline {subsubsection}{\numberline {6.2.3.12}swapButton}{16}{subsubsection.6.2.3.12}%
\contentsline {subsubsection}{\numberline {6.2.3.13}testButton}{16}{subsubsection.6.2.3.13}%
\contentsline {section}{\numberline {6.3}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Button\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Handler Class Reference}{16}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Member Function Documentation}{16}{subsection.6.3.1}%
\contentsline {subsubsection}{\numberline {6.3.1.1}inspect()}{16}{subsubsection.6.3.1.1}%
\contentsline {subsubsection}{\numberline {6.3.1.2}swap()}{17}{subsubsection.6.3.1.2}%
\contentsline {subsubsection}{\numberline {6.3.1.3}test()}{17}{subsubsection.6.3.1.3}%
\contentsline {section}{\numberline {6.4}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Coord Class Reference}{17}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Constructor \& Destructor Documentation}{17}{subsection.6.4.1}%
\contentsline {subsubsection}{\numberline {6.4.1.1}Coord()}{17}{subsubsection.6.4.1.1}%
\contentsline {subsection}{\numberline {6.4.2}Member Function Documentation}{18}{subsection.6.4.2}%
\contentsline {subsubsection}{\numberline {6.4.2.1}getLat()}{18}{subsubsection.6.4.2.1}%
\contentsline {subsubsection}{\numberline {6.4.2.2}getLong()}{18}{subsubsection.6.4.2.2}%
\contentsline {subsubsection}{\numberline {6.4.2.3}toString()}{18}{subsubsection.6.4.2.3}%
\contentsline {subsection}{\numberline {6.4.3}Member Data Documentation}{19}{subsection.6.4.3}%
\contentsline {subsubsection}{\numberline {6.4.3.1}latitude}{19}{subsubsection.6.4.3.1}%
\contentsline {subsubsection}{\numberline {6.4.3.2}longitude}{19}{subsubsection.6.4.3.2}%
\contentsline {section}{\numberline {6.5}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Direction\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Handler Class Reference}{19}{section.6.5}%
\contentsline {subsection}{\numberline {6.5.1}Member Function Documentation}{19}{subsection.6.5.1}%
\contentsline {subsubsection}{\numberline {6.5.1.1}loadUrl()}{19}{subsubsection.6.5.1.1}%
\contentsline {subsubsection}{\numberline {6.5.1.2}run()}{20}{subsubsection.6.5.1.2}%
\contentsline {section}{\numberline {6.6}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Graph Class Reference}{20}{section.6.6}%
\contentsline {subsection}{\numberline {6.6.1}Detailed Description}{21}{subsection.6.6.1}%
\contentsline {subsection}{\numberline {6.6.2}Constructor \& Destructor Documentation}{21}{subsection.6.6.2}%
\contentsline {subsubsection}{\numberline {6.6.2.1}Graph()}{21}{subsubsection.6.6.2.1}%
\contentsline {subsection}{\numberline {6.6.3}Member Function Documentation}{21}{subsection.6.6.3}%
\contentsline {subsubsection}{\numberline {6.6.3.1}addPath()}{21}{subsubsection.6.6.3.1}%
\contentsline {subsubsection}{\numberline {6.6.3.2}adj()}{21}{subsubsection.6.6.3.2}%
\contentsline {subsubsection}{\numberline {6.6.3.3}E()}{22}{subsubsection.6.6.3.3}%
\contentsline {subsubsection}{\numberline {6.6.3.4}edges()}{22}{subsubsection.6.6.3.4}%
\contentsline {subsubsection}{\numberline {6.6.3.5}getPath()}{22}{subsubsection.6.6.3.5}%
\contentsline {subsubsection}{\numberline {6.6.3.6}V()}{23}{subsubsection.6.6.3.6}%
\contentsline {subsection}{\numberline {6.6.4}Member Data Documentation}{23}{subsection.6.6.4}%
\contentsline {subsubsection}{\numberline {6.6.4.1}adj}{23}{subsubsection.6.6.4.1}%
\contentsline {subsubsection}{\numberline {6.6.4.2}E}{23}{subsubsection.6.6.4.2}%
\contentsline {subsubsection}{\numberline {6.6.4.3}V}{23}{subsubsection.6.6.4.3}%
\contentsline {section}{\numberline {6.7}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Index\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Min\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}PQ$<$ Key extends Comparable$<$ Key $>$.Heap\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Iterator Class Reference}{23}{section.6.7}%
\contentsline {subsection}{\numberline {6.7.1}Constructor \& Destructor Documentation}{24}{subsection.6.7.1}%
\contentsline {subsubsection}{\numberline {6.7.1.1}HeapIterator()}{24}{subsubsection.6.7.1.1}%
\contentsline {subsection}{\numberline {6.7.2}Member Function Documentation}{24}{subsection.6.7.2}%
\contentsline {subsubsection}{\numberline {6.7.2.1}hasNext()}{24}{subsubsection.6.7.2.1}%
\contentsline {subsubsection}{\numberline {6.7.2.2}next()}{24}{subsubsection.6.7.2.2}%
\contentsline {subsubsection}{\numberline {6.7.2.3}remove()}{24}{subsubsection.6.7.2.3}%
\contentsline {subsection}{\numberline {6.7.3}Member Data Documentation}{24}{subsection.6.7.3}%
\contentsline {subsubsection}{\numberline {6.7.3.1}copy}{25}{subsubsection.6.7.3.1}%
\contentsline {section}{\numberline {6.8}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Index\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Min\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}PQ$<$ Key extends Comparable$<$ Key $>$ Class Template Reference}{25}{section.6.8}%
\contentsline {subsection}{\numberline {6.8.1}Detailed Description}{26}{subsection.6.8.1}%
\contentsline {subsection}{\numberline {6.8.2}Constructor \& Destructor Documentation}{26}{subsection.6.8.2}%
\contentsline {subsubsection}{\numberline {6.8.2.1}IndexMinPQ()}{26}{subsubsection.6.8.2.1}%
\contentsline {subsection}{\numberline {6.8.3}Member Function Documentation}{26}{subsection.6.8.3}%
\contentsline {subsubsection}{\numberline {6.8.3.1}changeKey()}{27}{subsubsection.6.8.3.1}%
\contentsline {subsubsection}{\numberline {6.8.3.2}contains()}{27}{subsubsection.6.8.3.2}%
\contentsline {subsubsection}{\numberline {6.8.3.3}decreaseKey()}{27}{subsubsection.6.8.3.3}%
\contentsline {subsubsection}{\numberline {6.8.3.4}delete()}{28}{subsubsection.6.8.3.4}%
\contentsline {subsubsection}{\numberline {6.8.3.5}delMin()}{28}{subsubsection.6.8.3.5}%
\contentsline {subsubsection}{\numberline {6.8.3.6}exch()}{28}{subsubsection.6.8.3.6}%
\contentsline {subsubsection}{\numberline {6.8.3.7}greater()}{29}{subsubsection.6.8.3.7}%
\contentsline {subsubsection}{\numberline {6.8.3.8}increaseKey()}{29}{subsubsection.6.8.3.8}%
\contentsline {subsubsection}{\numberline {6.8.3.9}insert()}{30}{subsubsection.6.8.3.9}%
\contentsline {subsubsection}{\numberline {6.8.3.10}isEmpty()}{30}{subsubsection.6.8.3.10}%
\contentsline {subsubsection}{\numberline {6.8.3.11}iterator()}{30}{subsubsection.6.8.3.11}%
\contentsline {subsubsection}{\numberline {6.8.3.12}keyOf()}{30}{subsubsection.6.8.3.12}%
\contentsline {subsubsection}{\numberline {6.8.3.13}minIndex()}{31}{subsubsection.6.8.3.13}%
\contentsline {subsubsection}{\numberline {6.8.3.14}minKey()}{31}{subsubsection.6.8.3.14}%
\contentsline {subsubsection}{\numberline {6.8.3.15}sink()}{31}{subsubsection.6.8.3.15}%
\contentsline {subsubsection}{\numberline {6.8.3.16}size()}{32}{subsubsection.6.8.3.16}%
\contentsline {subsubsection}{\numberline {6.8.3.17}swim()}{32}{subsubsection.6.8.3.17}%
\contentsline {subsection}{\numberline {6.8.4}Member Data Documentation}{32}{subsection.6.8.4}%
\contentsline {subsubsection}{\numberline {6.8.4.1}keys}{32}{subsubsection.6.8.4.1}%
\contentsline {subsubsection}{\numberline {6.8.4.2}maxN}{32}{subsubsection.6.8.4.2}%
\contentsline {subsubsection}{\numberline {6.8.4.3}n}{33}{subsubsection.6.8.4.3}%
\contentsline {subsubsection}{\numberline {6.8.4.4}pq}{33}{subsubsection.6.8.4.4}%
\contentsline {subsubsection}{\numberline {6.8.4.5}qp}{33}{subsubsection.6.8.4.5}%
\contentsline {section}{\numberline {6.9}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Queue$<$ Item $>$.List\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Iterator Class Reference}{33}{section.6.9}%
\contentsline {subsection}{\numberline {6.9.1}Detailed Description}{33}{subsection.6.9.1}%
\contentsline {subsection}{\numberline {6.9.2}Member Function Documentation}{34}{subsection.6.9.2}%
\contentsline {subsubsection}{\numberline {6.9.2.1}hasNext()}{34}{subsubsection.6.9.2.1}%
\contentsline {subsubsection}{\numberline {6.9.2.2}next()}{34}{subsubsection.6.9.2.2}%
\contentsline {subsubsection}{\numberline {6.9.2.3}remove()}{34}{subsubsection.6.9.2.3}%
\contentsline {subsection}{\numberline {6.9.3}Member Data Documentation}{34}{subsection.6.9.3}%
\contentsline {subsubsection}{\numberline {6.9.3.1}current}{34}{subsubsection.6.9.3.1}%
\contentsline {section}{\numberline {6.10}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stack$<$ Item $>$.List\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Iterator Class Reference}{34}{section.6.10}%
\contentsline {subsection}{\numberline {6.10.1}Detailed Description}{35}{subsection.6.10.1}%
\contentsline {subsection}{\numberline {6.10.2}Member Function Documentation}{35}{subsection.6.10.2}%
\contentsline {subsubsection}{\numberline {6.10.2.1}hasNext()}{35}{subsubsection.6.10.2.1}%
\contentsline {subsubsection}{\numberline {6.10.2.2}next()}{35}{subsubsection.6.10.2.2}%
\contentsline {subsubsection}{\numberline {6.10.2.3}remove()}{35}{subsubsection.6.10.2.3}%
\contentsline {subsection}{\numberline {6.10.3}Member Data Documentation}{35}{subsection.6.10.3}%
\contentsline {subsubsection}{\numberline {6.10.3.1}current}{36}{subsubsection.6.10.3.1}%
\contentsline {section}{\numberline {6.11}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Queue$<$ Item $>$.Node Class Reference}{36}{section.6.11}%
\contentsline {subsection}{\numberline {6.11.1}Detailed Description}{36}{subsection.6.11.1}%
\contentsline {section}{\numberline {6.12}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stack$<$ Item $>$.Node Class Reference}{36}{section.6.12}%
\contentsline {subsection}{\numberline {6.12.1}Detailed Description}{36}{subsection.6.12.1}%
\contentsline {section}{\numberline {6.13}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Parse\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Worker Class Reference}{36}{section.6.13}%
\contentsline {subsection}{\numberline {6.13.1}Constructor \& Destructor Documentation}{37}{subsection.6.13.1}%
\contentsline {subsubsection}{\numberline {6.13.1.1}ParseWorker()}{37}{subsubsection.6.13.1.1}%
\contentsline {subsection}{\numberline {6.13.2}Member Function Documentation}{37}{subsection.6.13.2}%
\contentsline {subsubsection}{\numberline {6.13.2.1}doInBackground()}{37}{subsubsection.6.13.2.1}%
\contentsline {subsection}{\numberline {6.13.3}Member Data Documentation}{38}{subsection.6.13.3}%
\contentsline {subsubsection}{\numberline {6.13.3.1}bixiRed}{38}{subsubsection.6.13.3.1}%
\contentsline {section}{\numberline {6.14}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip Class Reference}{38}{section.6.14}%
\contentsline {subsection}{\numberline {6.14.1}Constructor \& Destructor Documentation}{39}{subsection.6.14.1}%
\contentsline {subsubsection}{\numberline {6.14.1.1}PastTrip()}{39}{subsubsection.6.14.1.1}%
\contentsline {subsection}{\numberline {6.14.2}Member Function Documentation}{39}{subsection.6.14.2}%
\contentsline {subsubsection}{\numberline {6.14.2.1}compareTo()}{39}{subsubsection.6.14.2.1}%
\contentsline {subsubsection}{\numberline {6.14.2.2}getDuration()}{40}{subsubsection.6.14.2.2}%
\contentsline {subsubsection}{\numberline {6.14.2.3}getEndCode()}{40}{subsubsection.6.14.2.3}%
\contentsline {subsubsection}{\numberline {6.14.2.4}getStartCode()}{40}{subsubsection.6.14.2.4}%
\contentsline {subsection}{\numberline {6.14.3}Member Data Documentation}{40}{subsection.6.14.3}%
\contentsline {subsubsection}{\numberline {6.14.3.1}duration}{41}{subsubsection.6.14.3.1}%
\contentsline {subsubsection}{\numberline {6.14.3.2}endCode}{41}{subsubsection.6.14.3.2}%
\contentsline {subsubsection}{\numberline {6.14.3.3}startCode}{41}{subsubsection.6.14.3.3}%
\contentsline {section}{\numberline {6.15}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Parser Class Reference}{41}{section.6.15}%
\contentsline {subsection}{\numberline {6.15.1}Member Function Documentation}{41}{subsection.6.15.1}%
\contentsline {subsubsection}{\numberline {6.15.1.1}parse()}{41}{subsubsection.6.15.1.1}%
\contentsline {section}{\numberline {6.16}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trips Class Reference}{42}{section.6.16}%
\contentsline {subsection}{\numberline {6.16.1}Member Function Documentation}{43}{subsection.6.16.1}%
\contentsline {subsubsection}{\numberline {6.16.1.1}addTrip()}{43}{subsubsection.6.16.1.1}%
\contentsline {subsubsection}{\numberline {6.16.1.2}getInstance()}{43}{subsubsection.6.16.1.2}%
\contentsline {subsubsection}{\numberline {6.16.1.3}getNextPath()}{43}{subsubsection.6.16.1.3}%
\contentsline {subsubsection}{\numberline {6.16.1.4}initialize()}{43}{subsubsection.6.16.1.4}%
\contentsline {subsubsection}{\numberline {6.16.1.5}sortPastTrips()}{44}{subsubsection.6.16.1.5}%
\contentsline {subsection}{\numberline {6.16.2}Member Data Documentation}{44}{subsection.6.16.2}%
\contentsline {subsubsection}{\numberline {6.16.2.1}i}{44}{subsubsection.6.16.2.1}%
\contentsline {subsubsection}{\numberline {6.16.2.2}instance}{44}{subsubsection.6.16.2.2}%
\contentsline {subsubsection}{\numberline {6.16.2.3}isSorted}{44}{subsubsection.6.16.2.3}%
\contentsline {subsubsection}{\numberline {6.16.2.4}pastTrips}{44}{subsubsection.6.16.2.4}%
\contentsline {section}{\numberline {6.17}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trips\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}B\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}U\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Merge\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sort Class Reference}{44}{section.6.17}%
\contentsline {subsection}{\numberline {6.17.1}Detailed Description}{45}{subsection.6.17.1}%
\contentsline {subsection}{\numberline {6.17.2}Member Function Documentation}{45}{subsection.6.17.2}%
\contentsline {subsubsection}{\numberline {6.17.2.1}less()}{45}{subsubsection.6.17.2.1}%
\contentsline {subsubsection}{\numberline {6.17.2.2}merge()}{45}{subsubsection.6.17.2.2}%
\contentsline {subsubsection}{\numberline {6.17.2.3}sort()}{47}{subsubsection.6.17.2.3}%
\contentsline {subsection}{\numberline {6.17.3}Member Data Documentation}{47}{subsection.6.17.3}%
\contentsline {subsubsection}{\numberline {6.17.3.1}aux}{47}{subsubsection.6.17.3.1}%
\contentsline {section}{\numberline {6.18}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Path Class Reference}{47}{section.6.18}%
\contentsline {subsection}{\numberline {6.18.1}Constructor \& Destructor Documentation}{48}{subsection.6.18.1}%
\contentsline {subsubsection}{\numberline {6.18.1.1}Path()}{48}{subsubsection.6.18.1.1}%
\contentsline {subsection}{\numberline {6.18.2}Member Function Documentation}{48}{subsection.6.18.2}%
\contentsline {subsubsection}{\numberline {6.18.2.1}addPastTrip()}{48}{subsubsection.6.18.2.1}%
\contentsline {subsubsection}{\numberline {6.18.2.2}getCount()}{50}{subsubsection.6.18.2.2}%
\contentsline {subsubsection}{\numberline {6.18.2.3}getDuration()}{50}{subsubsection.6.18.2.3}%
\contentsline {subsubsection}{\numberline {6.18.2.4}getEndIndex()}{50}{subsubsection.6.18.2.4}%
\contentsline {subsubsection}{\numberline {6.18.2.5}getStartIndex()}{51}{subsubsection.6.18.2.5}%
\contentsline {subsection}{\numberline {6.18.3}Member Data Documentation}{51}{subsection.6.18.3}%
\contentsline {subsubsection}{\numberline {6.18.3.1}count}{51}{subsubsection.6.18.3.1}%
\contentsline {subsubsection}{\numberline {6.18.3.2}duration}{51}{subsubsection.6.18.3.2}%
\contentsline {subsubsection}{\numberline {6.18.3.3}stations}{51}{subsubsection.6.18.3.3}%
\contentsline {section}{\numberline {6.19}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Paths Class Reference}{51}{section.6.19}%
\contentsline {subsection}{\numberline {6.19.1}Constructor \& Destructor Documentation}{52}{subsection.6.19.1}%
\contentsline {subsubsection}{\numberline {6.19.1.1}Paths()}{52}{subsubsection.6.19.1.1}%
\contentsline {subsection}{\numberline {6.19.2}Member Function Documentation}{52}{subsection.6.19.2}%
\contentsline {subsubsection}{\numberline {6.19.2.1}addPath()}{52}{subsubsection.6.19.2.1}%
\contentsline {subsubsection}{\numberline {6.19.2.2}getGraph()}{53}{subsubsection.6.19.2.2}%
\contentsline {subsubsection}{\numberline {6.19.2.3}getInstance()}{53}{subsubsection.6.19.2.3}%
\contentsline {subsubsection}{\numberline {6.19.2.4}getPath()}{53}{subsubsection.6.19.2.4}%
\contentsline {subsubsection}{\numberline {6.19.2.5}importPastTrips()}{53}{subsubsection.6.19.2.5}%
\contentsline {subsection}{\numberline {6.19.3}Member Data Documentation}{54}{subsection.6.19.3}%
\contentsline {subsubsection}{\numberline {6.19.3.1}graph}{54}{subsubsection.6.19.3.1}%
\contentsline {subsubsection}{\numberline {6.19.3.2}instance}{54}{subsubsection.6.19.3.2}%
\contentsline {subsubsection}{\numberline {6.19.3.3}stations}{54}{subsubsection.6.19.3.3}%
\contentsline {section}{\numberline {6.20}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Queue$<$ Item $>$ Class Template Reference}{54}{section.6.20}%
\contentsline {subsection}{\numberline {6.20.1}Member Function Documentation}{55}{subsection.6.20.1}%
\contentsline {subsubsection}{\numberline {6.20.1.1}dequeue()}{55}{subsubsection.6.20.1.1}%
\contentsline {subsubsection}{\numberline {6.20.1.2}enqueue()}{55}{subsubsection.6.20.1.2}%
\contentsline {subsubsection}{\numberline {6.20.1.3}isEmpty()}{56}{subsubsection.6.20.1.3}%
\contentsline {subsubsection}{\numberline {6.20.1.4}iterator()}{56}{subsubsection.6.20.1.4}%
\contentsline {subsubsection}{\numberline {6.20.1.5}size()}{56}{subsubsection.6.20.1.5}%
\contentsline {subsection}{\numberline {6.20.2}Member Data Documentation}{56}{subsection.6.20.2}%
\contentsline {subsubsection}{\numberline {6.20.2.1}first}{56}{subsubsection.6.20.2.1}%
\contentsline {subsubsection}{\numberline {6.20.2.2}last}{56}{subsubsection.6.20.2.2}%
\contentsline {subsubsection}{\numberline {6.20.2.3}n}{57}{subsubsection.6.20.2.3}%
\contentsline {section}{\numberline {6.21}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}SP Class Reference}{57}{section.6.21}%
\contentsline {subsection}{\numberline {6.21.1}Detailed Description}{57}{subsection.6.21.1}%
\contentsline {subsection}{\numberline {6.21.2}Constructor \& Destructor Documentation}{57}{subsection.6.21.2}%
\contentsline {subsubsection}{\numberline {6.21.2.1}SP()}{57}{subsubsection.6.21.2.1}%
\contentsline {subsection}{\numberline {6.21.3}Member Function Documentation}{58}{subsection.6.21.3}%
\contentsline {subsubsection}{\numberline {6.21.3.1}distTo()}{58}{subsubsection.6.21.3.1}%
\contentsline {subsubsection}{\numberline {6.21.3.2}hasPathTo()}{58}{subsubsection.6.21.3.2}%
\contentsline {subsubsection}{\numberline {6.21.3.3}pathTo()}{58}{subsubsection.6.21.3.3}%
\contentsline {subsubsection}{\numberline {6.21.3.4}relax()}{60}{subsubsection.6.21.3.4}%
\contentsline {subsection}{\numberline {6.21.4}Member Data Documentation}{60}{subsection.6.21.4}%
\contentsline {subsubsection}{\numberline {6.21.4.1}BIKE\_SWITCH\_PENALTY}{60}{subsubsection.6.21.4.1}%
\contentsline {subsubsection}{\numberline {6.21.4.2}distTo}{60}{subsubsection.6.21.4.2}%
\contentsline {subsubsection}{\numberline {6.21.4.3}PATH\_COUNT\_CUTOFF}{60}{subsubsection.6.21.4.3}%
\contentsline {subsubsection}{\numberline {6.21.4.4}PATH\_LEN\_MAX}{61}{subsubsection.6.21.4.4}%
\contentsline {subsubsection}{\numberline {6.21.4.5}PATH\_LEN\_MIN}{61}{subsubsection.6.21.4.5}%
\contentsline {subsubsection}{\numberline {6.21.4.6}pathTo}{61}{subsubsection.6.21.4.6}%
\contentsline {subsubsection}{\numberline {6.21.4.7}pq}{61}{subsubsection.6.21.4.7}%
\contentsline {section}{\numberline {6.22}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stack$<$ Item $>$ Class Template Reference}{61}{section.6.22}%
\contentsline {subsection}{\numberline {6.22.1}Detailed Description}{62}{subsection.6.22.1}%
\contentsline {subsection}{\numberline {6.22.2}Member Function Documentation}{62}{subsection.6.22.2}%
\contentsline {subsubsection}{\numberline {6.22.2.1}isEmpty()}{62}{subsubsection.6.22.2.1}%
\contentsline {subsubsection}{\numberline {6.22.2.2}iterator()}{62}{subsubsection.6.22.2.2}%
\contentsline {subsubsection}{\numberline {6.22.2.3}pop()}{63}{subsubsection.6.22.2.3}%
\contentsline {subsubsection}{\numberline {6.22.2.4}push()}{63}{subsubsection.6.22.2.4}%
\contentsline {subsubsection}{\numberline {6.22.2.5}size()}{63}{subsubsection.6.22.2.5}%
\contentsline {subsection}{\numberline {6.22.3}Member Data Documentation}{63}{subsection.6.22.3}%
\contentsline {subsubsection}{\numberline {6.22.3.1}first}{63}{subsubsection.6.22.3.1}%
\contentsline {subsubsection}{\numberline {6.22.3.2}n}{64}{subsubsection.6.22.3.2}%
\contentsline {section}{\numberline {6.23}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Station Class Reference}{64}{section.6.23}%
\contentsline {subsection}{\numberline {6.23.1}Constructor \& Destructor Documentation}{64}{subsection.6.23.1}%
\contentsline {subsubsection}{\numberline {6.23.1.1}Station()}{64}{subsubsection.6.23.1.1}%
\contentsline {subsection}{\numberline {6.23.2}Member Function Documentation}{65}{subsection.6.23.2}%
\contentsline {subsubsection}{\numberline {6.23.2.1}compareTo()}{65}{subsubsection.6.23.2.1}%
\contentsline {subsubsection}{\numberline {6.23.2.2}getCode()}{65}{subsubsection.6.23.2.2}%
\contentsline {subsubsection}{\numberline {6.23.2.3}getCoords()}{65}{subsubsection.6.23.2.3}%
\contentsline {subsubsection}{\numberline {6.23.2.4}getName()}{66}{subsubsection.6.23.2.4}%
\contentsline {subsection}{\numberline {6.23.3}Member Data Documentation}{66}{subsection.6.23.3}%
\contentsline {subsubsection}{\numberline {6.23.3.1}code}{66}{subsubsection.6.23.3.1}%
\contentsline {subsubsection}{\numberline {6.23.3.2}coords}{66}{subsubsection.6.23.3.2}%
\contentsline {subsubsection}{\numberline {6.23.3.3}name}{66}{subsubsection.6.23.3.3}%
\contentsline {section}{\numberline {6.24}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stations Class Reference}{66}{section.6.24}%
\contentsline {subsection}{\numberline {6.24.1}Member Function Documentation}{67}{subsection.6.24.1}%
\contentsline {subsubsection}{\numberline {6.24.1.1}addStation()}{67}{subsubsection.6.24.1.1}%
\contentsline {subsubsection}{\numberline {6.24.1.2}getIndex()}{67}{subsubsection.6.24.1.2}%
\contentsline {subsubsection}{\numberline {6.24.1.3}getInstance()}{68}{subsubsection.6.24.1.3}%
\contentsline {subsubsection}{\numberline {6.24.1.4}getStationByCode()}{68}{subsubsection.6.24.1.4}%
\contentsline {subsubsection}{\numberline {6.24.1.5}getStationByIndex()}{68}{subsubsection.6.24.1.5}%
\contentsline {subsubsection}{\numberline {6.24.1.6}getStations()}{69}{subsubsection.6.24.1.6}%
\contentsline {subsubsection}{\numberline {6.24.1.7}size()}{69}{subsubsection.6.24.1.7}%
\contentsline {subsubsection}{\numberline {6.24.1.8}sortStations()}{69}{subsubsection.6.24.1.8}%
\contentsline {subsection}{\numberline {6.24.2}Member Data Documentation}{69}{subsection.6.24.2}%
\contentsline {subsubsection}{\numberline {6.24.2.1}instance}{70}{subsubsection.6.24.2.1}%
\contentsline {subsubsection}{\numberline {6.24.2.2}isSorted}{70}{subsubsection.6.24.2.2}%
\contentsline {subsubsection}{\numberline {6.24.2.3}stations}{70}{subsubsection.6.24.2.3}%
\contentsline {section}{\numberline {6.25}algs.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stations\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Mergesort Class Reference}{70}{section.6.25}%
\contentsline {subsection}{\numberline {6.25.1}Detailed Description}{70}{subsection.6.25.1}%
\contentsline {subsection}{\numberline {6.25.2}Member Function Documentation}{71}{subsection.6.25.2}%
\contentsline {subsubsection}{\numberline {6.25.2.1}less()}{71}{subsubsection.6.25.2.1}%
\contentsline {subsubsection}{\numberline {6.25.2.2}merge()}{72}{subsubsection.6.25.2.2}%
\contentsline {subsubsection}{\numberline {6.25.2.3}mergeByName()}{72}{subsubsection.6.25.2.3}%
\contentsline {subsubsection}{\numberline {6.25.2.4}sort()}{73}{subsubsection.6.25.2.4}%
\contentsline {subsubsection}{\numberline {6.25.2.5}sortByName()}{73}{subsubsection.6.25.2.5}%
\contentsline {subsection}{\numberline {6.25.3}Member Data Documentation}{73}{subsection.6.25.3}%
\contentsline {subsubsection}{\numberline {6.25.3.1}aux}{73}{subsubsection.6.25.3.1}%
\contentsline {section}{\numberline {6.26}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stations\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Parser Class Reference}{74}{section.6.26}%
\contentsline {subsection}{\numberline {6.26.1}Member Function Documentation}{74}{subsection.6.26.1}%
\contentsline {subsubsection}{\numberline {6.26.1.1}parse()}{74}{subsubsection.6.26.1.1}%
\contentsline {section}{\numberline {6.27}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Test\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}UI Class Reference}{74}{section.6.27}%
\contentsline {subsection}{\numberline {6.27.1}Constructor \& Destructor Documentation}{75}{subsection.6.27.1}%
\contentsline {subsubsection}{\numberline {6.27.1.1}TestUI()}{75}{subsubsection.6.27.1.1}%
\contentsline {subsection}{\numberline {6.27.2}Member Function Documentation}{75}{subsection.6.27.2}%
\contentsline {subsubsection}{\numberline {6.27.2.1}append()}{75}{subsubsection.6.27.2.1}%
\contentsline {section}{\numberline {6.28}bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip Class Reference}{76}{section.6.28}%
\contentsline {subsection}{\numberline {6.28.1}Constructor \& Destructor Documentation}{76}{subsection.6.28.1}%
\contentsline {subsubsection}{\numberline {6.28.1.1}Trip()}{76}{subsubsection.6.28.1.1}%
\contentsline {subsection}{\numberline {6.28.2}Member Function Documentation}{76}{subsection.6.28.2}%
\contentsline {subsubsection}{\numberline {6.28.2.1}getDuration()}{77}{subsubsection.6.28.2.1}%
\contentsline {subsubsection}{\numberline {6.28.2.2}getRoute()}{77}{subsubsection.6.28.2.2}%
\contentsline {subsubsection}{\numberline {6.28.2.3}getUrl()}{77}{subsubsection.6.28.2.3}%
\contentsline {subsection}{\numberline {6.28.3}Member Data Documentation}{77}{subsection.6.28.3}%
\contentsline {subsubsection}{\numberline {6.28.3.1}end}{77}{subsubsection.6.28.3.1}%
\contentsline {subsubsection}{\numberline {6.28.3.2}route}{77}{subsubsection.6.28.3.2}%
\contentsline {subsubsection}{\numberline {6.28.3.3}sp}{78}{subsubsection.6.28.3.3}%
\contentsline {subsubsection}{\numberline {6.28.3.4}start}{78}{subsubsection.6.28.3.4}%
\contentsline {subsubsection}{\numberline {6.28.3.5}stations}{78}{subsubsection.6.28.3.5}%
\contentsline {chapter}{\numberline {7}File Documentation}{79}{chapter.7}%
\contentsline {section}{\numberline {7.1}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Binary\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Search.java File Reference}{79}{section.7.1}%
\contentsline {section}{\numberline {7.2}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Graph.java File Reference}{79}{section.7.2}%
\contentsline {section}{\numberline {7.3}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Index\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Min\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}PQ.java File Reference}{79}{section.7.3}%
\contentsline {section}{\numberline {7.4}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trips\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}B\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}U\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Merge\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sort.java File Reference}{80}{section.7.4}%
\contentsline {section}{\numberline {7.5}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Queue.java File Reference}{80}{section.7.5}%
\contentsline {section}{\numberline {7.6}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}SP.java File Reference}{80}{section.7.6}%
\contentsline {section}{\numberline {7.7}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stack.java File Reference}{80}{section.7.7}%
\contentsline {section}{\numberline {7.8}src/algs/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stations\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Mergesort.java File Reference}{81}{section.7.8}%
\contentsline {section}{\numberline {7.9}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}UI.java File Reference}{81}{section.7.9}%
\contentsline {subsection}{\numberline {7.9.1}Detailed Description}{81}{subsection.7.9.1}%
\contentsline {section}{\numberline {7.10}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Button\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Handler.java File Reference}{81}{section.7.10}%
\contentsline {subsection}{\numberline {7.10.1}Detailed Description}{82}{subsection.7.10.1}%
\contentsline {section}{\numberline {7.11}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Coord.java File Reference}{82}{section.7.11}%
\contentsline {subsection}{\numberline {7.11.1}Detailed Description}{82}{subsection.7.11.1}%
\contentsline {section}{\numberline {7.12}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Direction\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Handler.java File Reference}{82}{section.7.12}%
\contentsline {subsection}{\numberline {7.12.1}Detailed Description}{83}{subsection.7.12.1}%
\contentsline {section}{\numberline {7.13}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Parse\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Worker.java File Reference}{83}{section.7.13}%
\contentsline {subsection}{\numberline {7.13.1}Detailed Description}{83}{subsection.7.13.1}%
\contentsline {section}{\numberline {7.14}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.java File Reference}{83}{section.7.14}%
\contentsline {subsection}{\numberline {7.14.1}Detailed Description}{84}{subsection.7.14.1}%
\contentsline {section}{\numberline {7.15}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Parser.java File Reference}{84}{section.7.15}%
\contentsline {subsection}{\numberline {7.15.1}Detailed Description}{84}{subsection.7.15.1}%
\contentsline {section}{\numberline {7.16}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Past\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trips.java File Reference}{84}{section.7.16}%
\contentsline {subsection}{\numberline {7.16.1}Detailed Description}{85}{subsection.7.16.1}%
\contentsline {section}{\numberline {7.17}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Path.java File Reference}{85}{section.7.17}%
\contentsline {subsection}{\numberline {7.17.1}Detailed Description}{85}{subsection.7.17.1}%
\contentsline {section}{\numberline {7.18}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Paths.java File Reference}{85}{section.7.18}%
\contentsline {subsection}{\numberline {7.18.1}Detailed Description}{86}{subsection.7.18.1}%
\contentsline {section}{\numberline {7.19}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Station.java File Reference}{86}{section.7.19}%
\contentsline {subsection}{\numberline {7.19.1}Detailed Description}{86}{subsection.7.19.1}%
\contentsline {section}{\numberline {7.20}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stations.java File Reference}{86}{section.7.20}%
\contentsline {subsection}{\numberline {7.20.1}Detailed Description}{87}{subsection.7.20.1}%
\contentsline {section}{\numberline {7.21}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stations\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Parser.java File Reference}{87}{section.7.21}%
\contentsline {subsection}{\numberline {7.21.1}Detailed Description}{87}{subsection.7.21.1}%
\contentsline {section}{\numberline {7.22}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Test\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}UI.java File Reference}{87}{section.7.22}%
\contentsline {subsection}{\numberline {7.22.1}Detailed Description}{88}{subsection.7.22.1}%
\contentsline {section}{\numberline {7.23}src/bixi\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Trip.java File Reference}{88}{section.7.23}%
\contentsline {subsection}{\numberline {7.23.1}Detailed Description}{88}{subsection.7.23.1}%
\contentsline {chapter}{Index}{89}{section*.133}%
