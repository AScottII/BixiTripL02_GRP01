\documentclass[fullpage]{article}

%\setlength {\topmargin} {-1in}

%\setlength {\textheight} {8.6in}

%\setlength{\parindent}{1cm}

\usepackage{amsmath, amsfonts}
\usepackage[margin=1in]{geometry}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{color}
\usepackage{hyperref}
\usepackage{blindtext}
\usepackage{cancel}
\usepackage{float}
\usepackage{enumitem}
\usepackage{graphicx}    %  in the preamble
%\usepackage[acronym]{glossaries}

\usepackage[table]{xcolor}
\setlength{\tabcolsep}{18pt}
\renewcommand{\arraystretch}{1.5}
\renewcommand{\labelenumi}{\theenumi.}
\renewcommand{\labelenumii}{\theenumii.}
\renewcommand{\labelenumiii}{\theenumiii.}
\newcommand{\be}{\begin{enumerate}}
\newcommand{\ee}{\end{enumerate}}
\newcommand{\bi}{\begin{itemize}}
\newcommand{\ei}{\end{itemize}}
\newcommand{\bc}{\begin{center}}
\newcommand{\ec}{\end{center}}
\newcommand{\bv}{\begin{verbatim}}
\newcommand{\ev}{\end{verbatim}}
\newcommand{\ba}{\begin{align*}}
\newcommand{\ea}{\end{align*}}
\newcommand{\beq}{\begin{equation*}}
\newcommand{\eeq}{\end{equation*}}
\newcommand{\bs}{\begin{split}}
\newcommand{\es}{\end{split}}
\newcommand{\mname}[1]{\mbox{\sf #1}}
\newcommand{\pnote}[1]{{\langle \text{#1} \rangle}}
\renewcommand{\labelenumii}{\theenumii.}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=red,
    urlcolor=blue
}
\usepackage[round]{natbib}

\newcounter{acnum}
\newcommand{\actheacnum}{AC\theacnum}
\newcommand{\acref}[1]{AC\ref{#1}}

\newcounter{ucnum}
\newcommand{\uctheucnum}{UC\theucnum}
\newcommand{\uref}[1]{UC\ref{#1}}

\newcounter{mnum}
\newcommand{\mthemnum}{M\themnum}
\newcommand{\mref}[1]{M\ref{#1}}

\begin{document}

\vspace*{\fill}
\begin{center}

  {\huge \textbf{SE 3XA3: Test Plan}}\\
\hrulefill\\[2mm]
  {\huge \textbf{Coding Queens}}\\[2mm]
{\large \today}\\[15mm]
{\large
\underline{\textbf{BixiTripL02\_GRP01}}\\
\begin{tabular}{ c c }

 Cassie Baldin & baldic1\\ 
 Alan Scott & scott30\\
 Harsh Mahajan & mahajanh    
\end{tabular}
}

\end{center}

\vspace*{\fill}

%-----------END OF TITLE PAGE-------------------------------------------------------------
\newpage
\begingroup
\hypersetup{hidelinks}
\tableofcontents

\listoffigures
\listoftables
\endgroup
\newpage

\section*{Table of Revisions}
\begin{table}[h]
\centering
\begin{tabular}{| l | c | p{4cm}| p{5cm}|}
\hline
 \rowcolor{lightgray} 
\textbf{Version} & \textbf{Date(dd.mm.yyyy)} &\textbf{Author(s)} &\textbf{Description}\\
\hline
0 & 13.03.2022 & Harsh Mahajan & First draft assembled.\\
\hline
1 & 15.03.2022 & Alan Scott \newline Harsh Mahajan & Completed first version of the document.\\
\hline
2&16.03.2022& Cassidy Baldin & Added all contents of Appendix A.\\
\hline
\end{tabular}
\caption{Table of Revisions}
\end{table}


\section{Introduction}

Decomposing a system into modules is a commonly accepted approach to developing
software.  A module is a work assignment for a programmer or programming
team~\citep{ParnasEtAl1984}.  We advocate a decomposition
based on the principle of information hiding~\citep{Parnas1972a}.  This
principle supports design for change, because the ``secrets'' that each module
hides represent likely future changes.  Design for change is valuable in SC,
where modifications are frequent, especially during initial development as the
solution space is explored.  

Our design follows the rules layed out by \citet{ParnasEtAl1984}, as follows:
\begin{itemize}
\item System details that are likely to change independently should be the
  secrets of separate modules.
\item Each data structure is used in only one module.
\item Any other program that requires information stored in a module's data
  structures must obtain it by calling access programs belonging to that module.
\end{itemize}

After completing the first stage of the design, the Software Requirements
Specification (SRS), the Module Guide (MG) is developed~\citep{ParnasEtAl1984}. The MG
specifies the modular structure of the system and is intended to allow both
designers and maintainers to easily identify the parts of the software.  The
potential readers of this document are as follows:

\begin{itemize}
\item New project members: This document can be a guide for a new project member
  to easily understand the overall structure and quickly find the
  relevant modules they are searching for.
\item Maintainers: The hierarchical structure of the module guide improves the
  maintainers' understanding when they need to make changes to the system. It is
  important for a maintainer to update the relevant sections of the document
  after changes have been made.
\item Designers: Once the module guide has been written, it can be used to
  check for consistency, feasibility and flexibility. Designers can verify the
  system in various ways, such as consistency among modules, feasibility of the
  decomposition, and flexibility of the design.
\end{itemize}

The rest of the document is organized as follows. Section
\ref{SecChange} lists the anticipated and unlikely changes of the software
requirements. Section \ref{SecMH} summarizes the module decomposition that
was constructed according to the likely changes. Section \ref{SecConnection}
specifies the connections between the software requirements and the
modules. Section \ref{SecMD} gives a detailed description of the
modules. Section \ref{SecTM} includes two traceability matrices. One checks
the completeness of the design against the requirements provided in the SRS. The
other shows the relation between anticipated changes and the modules. Section
\ref{SecUse} describes the use relation between modules.

\section{Anticipated and Unlikely Changes} \label{SecChange}

This section lists possible changes to the system. According to the likeliness
of the change, the possible changes are classified into two
categories. Anticipated changes are listed in Section \ref{SecAchange}, and
unlikely changes are listed in Section \ref{SecUchange}.

\subsection{Anticipated Changes} \label{SecAchange}

Anticipated changes are the source of the information that is to be hidden
inside the modules. Ideally, changing one of the anticipated changes will only
require changing the one module that hides the associated decision. The approach
adapted here is called design for
change.

\begin{description}
\item[\refstepcounter{acnum} \actheacnum \label{ac1}:] The bike-sharing system, Bixi, may change their time frame for one bike-ride.
\item[\refstepcounter{acnum} \actheacnum \label{ac2}:] The bike-stations may change in source data.

\end{description}

\subsection{Unlikely Changes} \label{SecUchange}

The module design should be as general as possible. However, a general system is
more complex. Sometimes this complexity is not necessary. Fixing some design
decisions at the system architecture stage can simplify the software design. If
these decision should later need to be changed, then many parts of the design
will potentially need to be modified. Hence, it is not intended that these
decisions will be changed.

\begin{description}
\item[\refstepcounter{ucnum} \uctheucnum \label{uc1}:] Input/Output devices
  (Input: mouse-input and file(.csv), Output: a Google Maps link that opens in default web-browser).
\item[\refstepcounter{ucnum} \uctheucnum \label{uc2}:] There will always be a source of input data from Bixi, external to the software.
\item [\refstepcounter{ucnum} \uctheucnum \label{uc3}:] System will always be in Java and use Java swing GUI.
\item [\refstepcounter{ucnum} \uctheucnum \label{uc4}:] The system will always be functional on a Windows OS.
\end{description}

\newpage

\section{Module Hierarchy} \label{SecMH}

This section provides an overview of the module design. Modules are summarized
in a hierarchy decomposed by secrets in Table \ref{TblMH}. The modules listed
below, which are leaves in the hierarchy tree, are the modules that will
actually be implemented.

\begin{description}
\item [\refstepcounter{mnum} \mthemnum \label{m1}:] View Module
\item [\refstepcounter{mnum} \mthemnum \label{m2}:] Controller Module
\item [\refstepcounter{mnum} \mthemnum \label{m3}:] Model/Behavior-Hiding Module
\item [\refstepcounter{mnum} \mthemnum \label{m4}:] Software Decision Module
\end{description}
\subsection{System Architecture}
The system will be implemented in an MVC (Model-View-Controller) style architecture. The keywords and definitions are as follows:\\
\bi
    \item \textbf{MVC:} The model is manipulated by a controller that the user interacts with. The view updates with the model \& the user sees the results of their interaction via the view.
    \item \textbf{Model: }This is the central point of the system. The view displayed to the user is based upon the model.
    \item \textbf{View: }Part of system that displays information to the user.
    \item \textbf{Controller: }This updates all information stored in the controller. The user manipulates this component.
\ei



\newpage
\begin{table}[h!]
\centering
\begin{tabular}{p{0.3\textwidth} p{0.6\textwidth}}
\toprule
\textbf{Level 1} & \textbf{Level 2}\\
\midrule

{View Module} & UserInputModule \newline FrontendTestingModule \\
\midrule

\multirow{7}{0.3\textwidth}{Controller Module} & ParserModule\\
& PastTripImportModule\\
& StationsImportModule\\
& ButtonControllerModule\\
& PathGeneratorModule\\
\midrule

\multirow{7}{0.3\textwidth}{Model Module} & CoordinateModelModule\\
& PastTripModelModule\\
& RouteModelModule\\
& RouteAggregationModule\\
& StationModelModule\\
& StationAggregationModule\\
& TripModelModule\\
& PastTripAggregationModule\\
\midrule

\multirow{3}{0.3\textwidth}{Software Decision Module} & BinarySearch\\
& Graph\\
& MinPriorityQueue\\
& PastTripsSorter\\
& Queue\\
& ShortestPath\\
& Stack\\
& StationsSorter\\
\bottomrule

\end{tabular}
\caption{Module Hierarchy}
\label{TblMH}
\end{table}

\section{Connection Between Requirements and Design} \label{SecConnection}

The design of the system is intended to satisfy the requirements developed in
the SRS. In this stage, the system is decomposed into modules. The connection
between requirements and modules is listed in Table \ref{TblRT}.

\section{Module Decomposition} \label{SecMD}

Modules are decomposed according to the principle of ``information hiding''
proposed by \citet{ParnasEtAl1984}. The \emph{Secrets} field in a module
decomposition is a brief statement of the design decision hidden by the
module. The \emph{Services} field specifies \emph{what} the module will do
without documenting \emph{how} to do it. For each module, a suggestion for the
implementing software is given under the \emph{Implemented By} title. If the
entry is \emph{OS}, this means that the module is provided by the operating
system or by standard programming language libraries.  Also indicate if the
module will be implemented specifically for the software.

Only the leaf modules in the
hierarchy have to be implemented. If a dash (\emph{--}) is shown, this means
that the module is not a leaf and will not have to be implemented. Whether or
not this module is implemented depends on the programming language
selected.

\subsection{View Module (\ref{m1})}
\subsubsection{UserInputModule (M1.1)}\label{m1.1}
\begin{description}
\item[Secrets:] Format of the user input

\item[Services:] Provides the data flow of the program in accordance with the user's selections. 

\item[Implemented By:] BixiTripUI
\end{description}

\subsubsection{FrontendTestingModule (M1.2)}\label{m1.2}
\begin{description}
\item[Secrets:] Front end test case format

\item[Services:] Provides the user with a user interface that shows the various functionalities of the individual components. 

\item[Implemented By:] TestUI
\end{description}

\subsection{Controller Module (\ref{m2})}
\subsubsection{PathGeneratorModule (M2.1)}\label{m2.1}
\begin{description}
\item[Secrets:] Location of the input file

\item[Services:] Creates a path object representing a given route

\item[Implemented By:] DirectionHandler
\end{description}

\subsubsection{PastTripImportModule (M2.2)}\label{m2.2}
\begin{description}
\item[Secrets:] File format and location of past trips

\item[Services:] Reads and stores past transit trips for the program to use

\item[Implemented By:] PastTripParser
\end{description}

\subsubsection{ParserModule (M2.3)}\label{m2.3}
\begin{description}
\item[Secrets:] Format of previous routes in the past trips object

\item[Services:] Parses through past trips already imported to generate all possible paths the directions can take

\item[Implemented By:] ParseWorker
\end{description}

\subsubsection{StationsImportModule (M2.4)}\label{m2.4}
\begin{description}
\item[Secrets:] File format (location of transit stations)

\item[Services:] Reads and stores all transit stations provided in a .csv file and stores them for the program

\item[Implemented By:] StationsParser
\end{description}

\subsubsection{ButtonControllerModule (M2.5)}\label{m2.5}
\begin{description}
\item[Secrets:] Link to the source code

\item[Services:] Provides the functionality for the inspect, swap and test buttons.

\item[Implemented By:] ButtonHandler
\end{description}

\subsection{Model/Behavior-Hiding Module (\ref{m3})}

\subsubsection{CoordinateModelModule (M3.1)}\label{m3.1}
\begin{description}
\item[Secrets:] Format of the coordinate data type

\item[Services:] Stores the values of the coordinates as an abstract data type

\item[Implemented By:] Coord
\end{description}


\subsubsection{PastTripModelModule (M3.2)}\label{m3.2}
\begin{description}
\item[Secrets:] Format of the past trips

\item[Services:] Provides an abstract data type respesenting a past trip partaken by a commuter

\item[Implemented By:] PastTrip
\end{description}

\subsubsection{PastTripAggregationModule (M3.3)}\label{m3.3}
\begin{description}
\item[Secrets:] How the past trips are organised

\item[Services:] Provides an aggregation of past trips as well as internal functions to sort them

\item[Implemented By:] PastTrips
\end{description}


\subsubsection{RouteModelModule (M3.4)}\label{m3.4}
\begin{description}
\item[Secrets:] Format of a transit route

\item[Services:] Provides an abstract data type to represent a transit route

\item[Implemented By:] Path
\end{description}


\subsubsection{RouteAggregationModule (M3.5)}\label{m3.5}
\begin{description}
\item[Secrets:] How the routes are organised

\item[Services:] Provides a method for which transit routes can be aggregated and manipulated in such a way that the program may use them

\item[Implemented By:] Paths
\end{description}

\subsubsection{StationModelModule (M3.6)}\label{m3.6}
\begin{description}
\item[Secrets:] Format of station data

\item[Services:] Provides an abstract data type representing a transit station

\item[Implemented By:] Station
\end{description}


\subsubsection{StationAggregationModule (M3.7)}\label{m3.7}
\begin{description}
\item[Secrets:] How stations are organised by the program

\item[Services:] Provides an aggregation for all of the transit stations in the network

\item[Implemented By:] Stations
\end{description}

\subsubsection{TripModelModule (M3.8)}\label{m3.8}
\begin{description}
\item[Secrets:] How a trip is represented in the program

\item[Services:] Provides an abstract data type representing a trip by its start and end point, as well as its duration. 

\item[Implemented By:] Trip
\end{description}

\subsection{Software Decision Module (\ref{m4})}
This module was for the most part adoption straight from the original source code of BixiTrip. This is becuase this module implements various Data Structures \& Algorithms and we did not wish to reinvent the wheel.
\subsubsection{BinarySearch (M4.1)}\label{m4.1}
\begin{description}
\item[Secrets:] Binary search algorithm

\item[Services:] Provides the program with access to the binary search algorithm to search for stations

\item[Implemented By:] BinarySearch
\end{description}

\subsubsection{Graph (M4.2)}\label{m4.2}
\begin{description}
\item[Secrets:] Graph data type

\item[Services:] Provides the program with a node graph representation of different transit paths

\item[Implemented By:] Graph
\end{description}

\subsubsection{MinPriorityQueue (M4.3)}\label{m4.3}
\begin{description}
\item[Secrets:] Minumum priority queue data type

\item[Services:] Provides the program with a minumum priority queue to user with comparable and iterative values. 

\item[Implemented By:] IndexMiniPQ
\end{description}

\subsubsection{PastTripsSorter (M4.4)}\label{m4.4}
\begin{description}
\item[Secrets:] Mergesort data algorithm

\item[Services:] Provides the program with a method of sorting past trips but their indexes

\item[Implemented By:] PastTripsBUMergeSort
\end{description}

\subsubsection{Queue (M4.5)}\label{m4.5}
\begin{description}
\item[Secrets:] Queue data structure

\item[Services:] Provides the program with a queue to sort values in a FIFO pattern

\item[Implemented By:] Queue
\end{description}

\subsubsection{Stack (M4.6)}\label{m4.6}
\begin{description}
\item[Secrets:] Stack data structure

\item[Services:] Provides the program with an implementation of the stack data structure

\item[Implemented By:] Stack
\end{description}

\subsubsection{ShortestPath (M4.7)}\label{m4.7}
\begin{description}
\item[Secrets:] Shortest path algorithm for a node graph data algorithm

\item[Services:] Provides the program with the means to find a shortest path for an edge-weighted digraph

\item[Implemented By:] SP
\end{description}

\subsubsection{StationsSorter (M4.8)}\label{m4.8}
\begin{description}
\item[Secrets:] Mergesort data algorithm

\item[Services:] Provides the program with a means of sorting the Stations data type.

\item[Implemented By:] StationsMergeSort
\end{description}

\section{Traceability Matrix} \label{SecTM}

This section shows two traceability matrices: between the modules and the
requirements and between the modules and the anticipated changes.

% the table should use mref, the requirements should be named, use something
% like fref
\begin{table}[H]
\centering
\begin{tabular}{p{0.2\textwidth} p{0.6\textwidth}}
\toprule
\textbf{Req.} & \textbf{Modules}\\
\midrule
FR1 & M\ref{m1.1}, M\ref{m2.1}, M\ref{m3.7}, M\ref{m3.6}, M\ref{m3.8}, M\ref{m4.8}, M\ref{m4.1}, M\ref{m2.2}, M\ref{m3.5}, M\ref{m2.3}\\
FR2 & M\ref{m1.1}, M\ref{m2.1}, M\ref{m3.7}, M\ref{m3.6}, M\ref{m3.8}, M\ref{m4.8}, M\ref{m4.1}, M\ref{m2.2}, M\ref{m3.5}, M\ref{m2.3} \\
FR3 & M\ref{m1.1}, M\ref{m3.7}, M\ref{m3.3}, M\ref{m4.8}\\
FR4 & M\ref{m1.1}, M\ref{m2.5}\\
FR5 & NA\\
FR6 & M\ref{m1.1}, M\ref{m2.5}\\
FR7 & M\ref{m1.1}, M\ref{m1.2}, M\ref{m2.5}\\
FR8 & M\ref{m1.1}, M\ref{m3.7}, M\ref{m3.6}\\
FR9 & M\ref{m1.1}, M\ref{m3.7}, M\ref{m3.6}, M\ref{m2.1}\\
FR10 & M\ref{m1.1}, M\ref{m2.3}, M\ref{m3.5}, M\ref{m3.3}\\
FR11 & M\ref{m1.1}, M\ref{m2.3}, M\ref{m3.5}, M\ref{m3.3}\\
\bottomrule
\end{tabular}
\caption{Trace Between Requirements and Modules}
\label{TblRT}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{p{0.2\textwidth} p{0.6\textwidth}}
\toprule
\textbf{AC} & \textbf{Modules}\\
\midrule
\acref{ac1} & \mref{m4.7}\\
\acref{ac2} & \mref{m2.2}, \mref{m2.4}\\
\bottomrule
\end{tabular}
\caption{Trace Between Anticipated Changes and Modules}
\label{TblACT}
\end{table}
\newpage
\section{Use Hierarchy Between Modules} \label{SecUse}

In this section, the uses hierarchy between modules is
provided. \citet{Parnas1978} said of two programs A and B that A {\em uses} B if
correct execution of B may be necessary for A to complete the task described in
its specification. That is, A {\em uses} B if there exist situations in which
the correct functioning of A depends upon the availability of a correct
implementation of B.  Figure \ref{FigUH} illustrates the use relation between
the modules. It can be seen that the graph is a directed acyclic graph
(DAG). Each level of the hierarchy offers a testable and usable subset of the
system, and modules in the higher level of the hierarchy are essentially simpler
because they use modules from the lower levels.

\begin{figure}[h!] \label{FigUH}
    \centering
    \includegraphics[scale=0.5]{Module Hierarchy.png}
    \caption{Use Hierarchy of BixiTrip}
    \label{fig:my_label}
\end{figure}
\appendix
\section{Gantt Chart}\label{gantt}

The link to the gantt chart can be found  \href{run:../ProjectSchedule/BixiTripGanttChart.gan}{here}(You need to pull all files on your local system for this to work).
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\linewidth]{Proj_Schedule.png}
    \caption{Gantt Chart}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\linewidth]{Resources.png}
    \caption{Resources for Project}
  \end{center}
\end{figure}

\subsection{Testing Schedule}

All team members will be contributing to the testing of the project as follows:
\begin{enumerate}
    \item Testing FRs - All team members will be contributing, with Alan Scott leading the team. 
    \item Testing NFRs - Harsh Mahajan and Cassidy Baldin will be testing the NFRs of the system. 
    \item Testing Module 1 - Cassidy Baldin
    \item Testing Module 2 - Alan Scott
    \item Testing Module 3 - Cassidy Baldin
    \item Testing Module 4 - Harsh Mahajan
\end{enumerate}

The team will be testing using the proposed tests outlined in the Testing Plan, with the team consistently documenting the test cases and their results. Each section will be broken down according to that document. Proposed dates for each testing section is outlined in the Gantt chart shown above. 

%\section*{References}

\bibliographystyle {plain}
\bibliography {MG}

\end{document}


