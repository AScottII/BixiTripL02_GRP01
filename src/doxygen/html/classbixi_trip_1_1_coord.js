var classbixi_trip_1_1_coord =
[
    [ "Coord", "classbixi_trip_1_1_coord.html#aed1287cb6853d2ef38a2e6ffcc19ebfc", null ],
    [ "getLat", "classbixi_trip_1_1_coord.html#aad3d3d4823926ead1df2de3d398adfaa", null ],
    [ "getLong", "classbixi_trip_1_1_coord.html#a0c3a40d339bc01763abd836c5faa9b4e", null ],
    [ "toString", "classbixi_trip_1_1_coord.html#ad768fa5438274e3f7d80ed3873bcac1f", null ],
    [ "latitude", "classbixi_trip_1_1_coord.html#a7aa05c4d6e49345434163f93ec54a7f2", null ],
    [ "longitude", "classbixi_trip_1_1_coord.html#aab11386135b93bd9c1db4cc6b3ce5dd2", null ]
];