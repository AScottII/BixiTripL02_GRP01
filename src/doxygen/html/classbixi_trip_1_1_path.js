var classbixi_trip_1_1_path =
[
    [ "Path", "classbixi_trip_1_1_path.html#a478af1ebc393303957d3067e337617a6", null ],
    [ "addPastTrip", "classbixi_trip_1_1_path.html#a4f5902c42f0ba174bc1f693f10c28c74", null ],
    [ "getCount", "classbixi_trip_1_1_path.html#a905cdc7ef904619922526ab62f79889f", null ],
    [ "getDuration", "classbixi_trip_1_1_path.html#ac67135339c088f7aaa18a9633fba8bbb", null ],
    [ "getEndIndex", "classbixi_trip_1_1_path.html#a3f6ec895653f0d0a41417765e42a33d7", null ],
    [ "getStartIndex", "classbixi_trip_1_1_path.html#a9947f9362245f24fb84deb5f4095351e", null ],
    [ "count", "classbixi_trip_1_1_path.html#a2c87d07fade676e2ccfb4de8e3ffdf79", null ],
    [ "duration", "classbixi_trip_1_1_path.html#a29402f41cb9bbb3698d8d415a4bfe2df", null ],
    [ "stations", "classbixi_trip_1_1_path.html#ad2f674a61366f4c64eccb8a1713ede5a", null ]
];