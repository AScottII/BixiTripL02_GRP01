var classbixi_trip_1_1_station =
[
    [ "Station", "classbixi_trip_1_1_station.html#aeec81504f0df21352a2eb0a1df35d82a", null ],
    [ "compareTo", "classbixi_trip_1_1_station.html#a8a7b49669ae5411deaea60357aa530f3", null ],
    [ "getCode", "classbixi_trip_1_1_station.html#aa384beb49ffc6c32621b6c09f857b418", null ],
    [ "getCoords", "classbixi_trip_1_1_station.html#aa53876b1b5273ad90f37eeff8a08feee", null ],
    [ "getName", "classbixi_trip_1_1_station.html#a56505f62bb93e86f1ff2c28c1088c247", null ],
    [ "code", "classbixi_trip_1_1_station.html#ae377cdacc231604a2138285dac04f553", null ],
    [ "coords", "classbixi_trip_1_1_station.html#a2ef139fc6669b954c41b9552ec4a4a87", null ],
    [ "name", "classbixi_trip_1_1_station.html#a0c17212ae80f456eca359d32e938c0e7", null ]
];