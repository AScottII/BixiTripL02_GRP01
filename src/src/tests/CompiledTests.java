package tests;

import bixiTrip.Coord;
import bixiTrip.PastTrip;
import bixiTrip.PastTrips;
import bixiTrip.Path;
import bixiTrip.Station;
import bixiTrip.Stations;

import java.util.ArrayList;

/**
 * @file CompiledTests.java
 * @author Alan Scott
 * @brief This class contains all the necessary test methods to work with the front end
 */
public class CompiledTests {
	int numCorrect = 0;
	int totalTests = 0;
	
	/**
	 * @brief Coord test method
	 * @return Formatted String indicator
	 */
	public String[] testCoord() {
		numCorrect = 0;
		totalTests = 2;
		Coord coord1 = new Coord(43.2609, -79.9192);
		numCorrect += (coord1.getLat() == 43.2609) ? 1 : 0;
		numCorrect += (coord1.getLong() == -79.9192) ? 1 : 0;
		String[] out = {"Coord.java",(numCorrect == totalTests) ? "passes" : "fails", numCorrect + "/" + totalTests};
		return out;
	}
	
	/**
	 * @brief Station test method
	 * @return Formatted String indicator
	 */
	public String[] testStation() {
		Coord coord1 = new Coord(43.2609, -79.9192);
		Coord coord2 = new Coord(-20.2984, 0.5748);
		Coord coord3 = new Coord(-3.1415, 9.2677);
		Station test1 = new Station(3056, "Station 1", coord1);
		Station test2 = new Station(5049, "Station 2", coord2);
		Station test3 = new Station(1, "Station 3", coord3);
		numCorrect = 0;
		totalTests = 5;
		
		numCorrect += (test1.getCode()==3056) ? 1 : 0;
		numCorrect += (test1.getName().equals("Station 1")) ? 1 : 0;
		numCorrect += (test1.getCoords()==coord1) ? 1 : 0;
		numCorrect += (test1.compareTo(test2)==-1) ? 1 : 0;
		numCorrect += (test2.compareTo(test3)==1) ? 1 : 0;
		
		String[] out = {"Station.java",(numCorrect == totalTests) ? "passes" : "fails", numCorrect + "/" + totalTests};
		return out;
	}
	
	/**
	 * @brief Stations test method
	 * @return Formatted String indicator
	 */
	public String[] testStations() {
		Stations stations = new Stations();
		Station station1 = new Station(43, "Station 1", new Coord(4.5843, 9.2940));
		Station station2 = new Station(33, "Station 2", new Coord(69.5435, -12.2423));
		stations.addStation(station1);
		stations.addStation(station2);
		numCorrect = 0;
		totalTests = 4;
		
		numCorrect += (stations.getStationByCode(43).getCode() == 43) ? 1 : 0;
		numCorrect += (stations.getIndex(43)==1) ? 1 : 0;
		
		try {
			stations.getStationByCode(11);
		} catch(IndexOutOfBoundsException e1){
			numCorrect += 1;
		}
		
		try {
			stations.getStationByIndex(3);
		} catch(IndexOutOfBoundsException e2) {
			numCorrect += 1;
		}
		
		String[] out = {"Stations.java",(numCorrect == totalTests) ? "passes" : "fails", numCorrect + "/" + totalTests};
		return out;
	}
	
	/**
	 * @brief Path test method
	 * @return Formatted String indicator
	 */
	public String[] testPath() {
		Coord coord = new Coord(0, 0);
		Station station1 = new Station(123, "station1", coord);
		Station station2 = new Station(456, "station2", coord);
		Stations stations = Stations.getInstance();
		stations.addStation(station1);
		stations.addStation(station2);
		PastTrip pastTrip1 = new PastTrip(123, 456, 12);
		PastTrip pastTrip2 = new PastTrip(123, 456, 18);
		PastTrip pastTrip3 = new PastTrip(1223, 456, 12);
		PastTrip pastTrip4 = new PastTrip(0, 0, 0);
		Path path = new Path(pastTrip1);
		numCorrect = 0;
		totalTests = 7;
		
		numCorrect += (123  == path.getStartCode()) ? 1 : 0;
		path.addPastTrip(pastTrip2);
		numCorrect += (123==path.getStartCode()) ? 1 : 0;
		numCorrect += (stations.getIndex(123)==path.getStartIndex()) ? 1 : 0;
		numCorrect += (stations.getIndex(456)==path.getEndIndex()) ? 1 : 0;
		Path path0 = new Path(pastTrip4);
		numCorrect += (path0.getDuration()==0) ? 1 : 0;
		numCorrect += (path.getDuration()==15) ? 1 : 0;
		
		try {
			path.addPastTrip(pastTrip3);
		} catch(IllegalArgumentException e3) {
			numCorrect += 1;
		}
		
		String[] out = {"Path.java",(numCorrect == totalTests) ? "passes" : "fails", numCorrect + "/" + totalTests};
		return out;
	}
	
	/**
	 * @brief PastTrip test method
	 * @return Formatted String indicator
	 */
	public String[] testPastTrip() {
		PastTrip test1 = new PastTrip(500, 1000, 180);
		PastTrip test2 = new PastTrip(673, 539, 300);
		PastTrip test3 = new PastTrip(673, 539, 730);
		numCorrect = 0;
		totalTests = 8;
		
		numCorrect += (test1.getStartCode() == 500) ? 1 : 0;
		numCorrect += (test2.getStartCode() == 673) ? 1 : 0;
		numCorrect += (test1.getEndCode() == 1000) ? 1 : 0;
		numCorrect += (test2.getEndCode() == 539) ? 1 : 0;
		numCorrect += (test1.getDuration() == 180) ? 1 : 0;
		numCorrect += (test2.getDuration() == 300) ? 1 : 0;
		numCorrect += (test1.compareTo(test2) == -1) ? 1 : 0;
		numCorrect += (test2.compareTo(test3) == 0) ? 1 : 0;
		
		String[] out = {"PastTrip.java",(numCorrect == totalTests) ? "passes" : "fails", numCorrect + "/" + totalTests};
		return out;
	}
	
	/**
	 * @brief PastTrips test method
	 * @return Formatted String indicator
	 */
	public String[] testPastTrips() {
		PastTrips pastTrips = new PastTrips();
		PastTrips pastTrips2 = new PastTrips();
		PastTrip test1 = new PastTrip(500, 1000, 180);
		PastTrip test2 = new PastTrip(673, 539, 300);
		PastTrip test3 = new PastTrip(5743, 599, 730);
		pastTrips.addTrip(test1);
		pastTrips.addTrip(test2);
		pastTrips.addTrip(test3);
		pastTrips2.addTrip(test2);
		pastTrips2.addTrip(test1);
		pastTrips2.addTrip(test3);
		numCorrect = 0;
		totalTests = 6;
		
		numCorrect += (pastTrips.getNextPath().get(0).getStartCode() == 500) ? 1 : 0;
		numCorrect += (pastTrips.getNextPath().get(0).getStartCode() == 673) ? 1 : 0;
		numCorrect += (pastTrips2.getNextPath().get(0).getStartCode() == 500) ? 1 : 0;
		numCorrect += (pastTrips2.getNextPath().get(0).getStartCode() == 673) ? 1 : 0;
		numCorrect += (pastTrips.getNextPath().get(0).getEndCode() == 599) ? 1 : 0;
		numCorrect += (pastTrips2.getNextPath().get(0).getDuration() == 730) ? 1 : 0;
		
		String[] out = {"PastTrip.java",(numCorrect == totalTests) ? "passes" : "fails", numCorrect + "/" + totalTests};
		return out;
	}
}
