package bixiTrip;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @file StationsParser.java
 * @author Alan Scott
 * @brief Station parsing helper class
 */
public class StationsParser {
	
	/**
	 * @brief Stations static parsing method
	 * @param dirPath
	 * @return fully populated Stations object
	 * @throws FileNotFoundException If the file is not found at the given directory
	 * 
	 * This method takes in the location of the stations .csv file, throwing an exception if it
	 * cannot be found. For each line in the file, the input is split along commas, and these values are used 
	 * to create new stations. These stations are added to a Stations object, which is returned when the file
	 * is fully traversed.
	 */
	public static Stations parse(String dirPath) throws FileNotFoundException {
		Stations stations = Stations.getInstance();

		File file = new File(dirPath);

		try {
			Scanner input = new Scanner(file);
			input.nextLine();

			// Read each line of the file and split by comma regex
			while (input.hasNext()) {
				String station = input.nextLine();
				String[] tempArray1 = station.split(",");

				// Local variables to store attributes of each station
				int code = Integer.parseInt(tempArray1[0]);

				String name = tempArray1[1];

				double latitude = Double.parseDouble(tempArray1[2]);
				double longitude = Double.parseDouble(tempArray1[3]);
				Coord coords = new Coord(latitude, longitude);

				// Add a station to stations object
				stations.addStation(new Station(code, name, coords));
			}
			input.close();
		}

		// Catch block
		catch (FileNotFoundException e) {
			throw e;
		}
		return stations;
	}
}
