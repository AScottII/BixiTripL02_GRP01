package bixiTrip;

import java.util.ArrayList;

/**
 * @file PastTrips.java
 * @brief Aggregate data type for PastTrip
 * @author Alan Scott
 */
public class PastTrips {

	/** Uninstantiated version of this class to be populated */
	private static PastTrips instance = null;
	/** Boolean representing the state of the collection as being either sorter or unsorted */
	private boolean isSorted = true;
	/** ArrayList representing the collection of past trips within this object */
	private final ArrayList<PastTrip> pastTrips = new ArrayList<PastTrip>();
	/** integer representing the current number of past trips in this object, initially 0 */
	private int i = 0;

	/**
	 * @brief PastTrips instance generator
	 * @return Current instance of the PastTrips class
	 */
	public static PastTrips getInstance() {
		if (instance == null) {
			instance = new PastTrips();
		}
		return instance;
	}

	/**
	 * @brief PastTrips sorting method
	 * 
	 * This method uses the specialized MergeSort algorithm to sort the past trips
	 */
	private void sortPastTrips() {
		algs.PastTripsBUMergeSort.sort(pastTrips);
		isSorted = true;
	}

	/**
	 * 
	 * @brief Best path generator
	 * @return List of past trips
	 * 
	 * Given a sorted set of past trips, this method returns a set of all possible past trips.
	 */
	public ArrayList<PastTrip> getNextPath() {
		ArrayList<PastTrip> nextTrips = new ArrayList<PastTrip>();
		if (!isSorted) {
			sortPastTrips();
		}

		if (i >= pastTrips.size() && nextTrips.size() < 1) {
//			System.out.println(
//					NumberFormat.getNumberInstance(Locale.US).format(pastTrips.size()) + " past trips parsed.");
			return null;
		}
		
		int start = pastTrips.get(i).getStartCode();
		int end = pastTrips.get(i).getEndCode();
		while (i < pastTrips.size() &&
			   start == pastTrips.get(i).getStartCode() &&
			   end == pastTrips.get(i).getEndCode()) 
		{
			nextTrips.add(pastTrips.get(i));
			i++;
		}
	
		return nextTrips;
	}

	/**
	 * @brief Past trip append method
	 * 
	 * This method adds a new past trip into the collection, then marks the collection as unsorted
	 */
	public void addTrip(PastTrip trip) {
		pastTrips.add(trip);
		isSorted = false;
	}

	/**
	 * @brief Function to move the count variable back to the first index.
	 */
	public void initialize() {
		i = 0;
	}
}
