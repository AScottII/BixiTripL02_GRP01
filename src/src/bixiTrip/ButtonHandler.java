package bixiTrip;

import java.awt.Desktop;
import java.net.URI;

import javax.swing.JComboBox;

/**
 * @file ButtonHandler.java
 * @author Alan Scott
 * @brief This class provides the static functionality for the 3 new buttons
 */
public class ButtonHandler {

	/**
	 * @brief This method controls the functionality of the swap button, swaps the indexes of the combo boxes
	 * @param startStationComboBox GUI combo box passed in as argument
	 * @param endStationComboBox GUI combo box passed in as argument
	 * 
	 * This method gets the indexes from each combo box, then sets their indexes corresponding to the
	 * index in the other combo box
	 */
	public static void swap(JComboBox startStationComboBox, JComboBox endStationComboBox) {
		int int1 = startStationComboBox.getSelectedIndex();
		int int2 = endStationComboBox.getSelectedIndex();
		startStationComboBox.setSelectedIndex(int2);
		endStationComboBox.setSelectedIndex(int1);
	}
	
	/**
	 * @brief Inspect code driver method
	 * @exception Exception if user's desktop browser is unsupported
	 * 
	 * This method opens the URL to the repo in the user's browser. Checks that the desktop is supported
	 * and that the user's desktop is capable of browsing online
	 */
	public static void inspect() {
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
			try {
				Desktop.getDesktop().browse(new URI("https://gitlab.cas.mcmaster.ca/scotta30/BixiTripL02_GRP01"));
			} catch (Exception f) {
				f.printStackTrace();
				return;
			}
		}
	}
	
	/**
	 * @brief testButton handler class. Instatiates a new TestUI class
	 */
	public static void test() {
		new TestUI();
	}
}
