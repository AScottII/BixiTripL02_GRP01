package bixiTrip;

import java.util.ArrayList;

/**
 * @file Stations.java
 * @brief Aggregate class for Station
 * @author Alan Scott
 */

public class Stations {

	/** Singleton instance of this class, initially null */
	private static Stations instance = null;
	/** Boolean representation of whether the collection is sorted or not */
	private boolean isSorted = true;
	/** Collection of stations this class represents */
	private final ArrayList<Station> stations = new ArrayList<Station>();

	/**
	 * @brief Constructor for a singleton Stations() object.
	 * @return Returns a single instance of the Stations object, creates a new one if
	 * one does not exist
	 */
	public static Stations getInstance() {
		if (instance == null)
			instance = new Stations();
		return instance;
	}

	/**
	 * @brief Private method to sort the Stations stored in the object.
	 */
	private void sortStations() {
		algs.StationsMergesort.sort(stations);
		isSorted = true;
	}

	/**
	 * @brief Function to find a station based on its code in a Stations object.
	 * @param code Integer code that represents the station to be found.
	 * @return Returns the station with the equivalent code.
	 * @throws ArrayIndexOutOfBoundsException If the station code does not exist in station list
	 * 
	 * This method gets the station by its code. First sorts the stations list if it is not
	 * yet sorted, then performs a binary search for the station. If the station is not found, throw an
	 * exception.
	 */
	public Station getStationByCode(int code) {
		if (!isSorted) {
			sortStations();
		}
		int index = algs.BinarySearch.stationSearch(stations, code);
		try {
			return stations.get(index);
		} catch (ArrayIndexOutOfBoundsException e) {
			// catch out-of-bounds exception
			throw e;
		}
	}

	/**
	 * @brief Function to find a station based on its code in a Stations object.
	 * @param index Integer code that represents the index position of the station
	 *              requested.
	 * @throws Exception If station index isn't found
	 * @return Returns the station at the given index.
	 * 
	 * Sorts the stations if they are not yet sorted, then returns the given station by its index.
	 * If the index does not exist, throws an exception.
	 */
	public Station getStationByIndex(int index) {
		if (!isSorted) {
			sortStations();
		}
		try {
			return stations.get(index);
		} catch (Exception e) {
			// catch out-of-bounds exception
			throw e;
		}
	}

	/**
	 * @brief Function to find a station's index in a Stations object.
	 * 
	 * @param code Integer code that represents the station to be found.
	 * @return Returns an int representing the index position.
	 */
	public int getIndex(int code) {
		if (!isSorted) {
			sortStations();
		}
		// convert to an array to be searched
		int index = algs.BinarySearch.stationSearch(stations, code);
		if (index < 0)
			throw new IndexOutOfBoundsException("Station not found.");
		return index;
	}

	/**
	 * @brief Add a Station object to Stations.
	 * 
	 * @param station A Station object to be added to the Stations data type.
	 */
	public void addStation(Station station) {
		// add station to object
		stations.add(station);
		isSorted = false;
	}

	/**
	 * @brief Function to return the stations stored in the instance of Stations.
	 * 
	 * @return Returns ArrayList of stations stored.
	 */
	public ArrayList<Station> getStations() {
		// sort list if not sorted
		if (!isSorted) {
			sortStations();
		}
		return stations;
	}

	/**
	 * @brief Function to return the number of stations stored in the instance of Stations.
	 * 
	 * @return Returns number of stations stored.
	 */
	public int size() {
		return stations.size();
	}
}
