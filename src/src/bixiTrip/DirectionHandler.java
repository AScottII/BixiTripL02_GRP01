package bixiTrip;

import java.awt.Color;
import java.awt.Desktop;
import java.net.URI;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * @file DirectionHandler.java
 * @author Alan Scott
 * @brief Driver for the direction button, calculates route.
 * 
 * This class provides the functionality for the directions button. The class contains static methods
 * for both computing the route and generating a google maps route.
 */
public class DirectionHandler {
	
	/**
	 * @brief Route generating method
	 * @param statusField status field passed from BixiTripUI
	 * @param startStationComboBox start station selection combo box passed from BixiTripUI
	 * @param endStationComboBox end station selection combo box passed from BixiTripUI
	 * @param stationsList generated list of stations from BixiTripUI
	 * 
	 * This method performs checks to see if the user has entered valid inputs into the combo boxes. 
	 * It then finds the codes for the entered stations and checks it against past routes. It then selects 
	 * the best past route and generates a path and the google maps url for this path. 
	 */
	public static void run(JTextField statusField, JComboBox startStationComboBox, JComboBox endStationComboBox, ArrayList<Station> stationsList) {
		Stations stations = Stations.getInstance();
		int startCode, endCode;
		Station start, end;
		Trip mainTrip;
		final Color bixiRed = new Color(213, 43, 30);
		final Color error = new Color(255, 0, 0);

		// check that two stations have been selected
		if (startStationComboBox.getSelectedItem() == "" || endStationComboBox.getSelectedItem() == "") {
			statusField.setForeground(error);
			statusField.setText("One or more selections are incorrect. Please select a start and end station.");
			return;
		}

		// check that each station is unique
		if (startStationComboBox.getSelectedIndex() == endStationComboBox.getSelectedIndex()) {
			statusField.setForeground(error);
			statusField.setText("Start and Destination cannot be the same station.");
			return;
		}

		// find start and end code for stations
		startCode = stationsList.get(startStationComboBox.getSelectedIndex() - 1).getCode();
		start = stations.getStationByCode(startCode);
		endCode = stationsList.get(endStationComboBox.getSelectedIndex() - 1).getCode();
		end = stations.getStationByCode(endCode);

		// create trip
		mainTrip = new Trip(start, end);
		String url = mainTrip.getUrl();
		statusField.setForeground(Color.black);
		statusField.setText("Trip from " + start.getName() + " to " + end.getName() + " found.");

		int result = JOptionPane.showConfirmDialog(null,"Open directions in Google Maps?", "Confirm", JOptionPane.YES_NO_OPTION);
		switch(result) {
		case(JOptionPane.YES_OPTION): //if user selects the 'Yes' option, go to google maps
			loadUrl(url); 
		default: //if user selects 'No' option, do nothing
			return;
		}
		
		
	}
	
	/**
	 * @brief Method to open a url in the user's browser
	 * @param url String url representing the path in google maps
	 * 
	 * This method receives a url input and attempts to open it in the browser. To do this, it first
	 * checks that the user's desktop is supported by java, and that the user's desktop is able to browse the internet. 
	 * Once this check is complete, it opens the link in the user's default browser.
	 */
	private static void loadUrl(String url) {
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (Exception f) {
				f.printStackTrace();
				return;
			}
		}
		return;
	}
}

