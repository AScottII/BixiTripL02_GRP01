package bixiTrip;

import java.io.*;
import java.util.Scanner;

/**
 * @file PastTripParser.java
 * @author Alan Scott
 * @brief Helper class to populate PastTrips with past trips
 */
public class PastTripParser {

	/**
	 * @brief PastTrips parsing helper method
	 * @param dirPath path to the directory containing the .csv file containing past commuter trips
	 * @return a populated version of PastTrips
	 * @throws FileNotFoundException If the given file is not found in the given directory
	 * 
	 * This method starts by getting the empty copy of PastTrips. It then locates the file passed by
	 * the BixiTripUI class, throwing an error if this file cannot be found. It then goes through each line of the
	 * .csv file, selecting specific values of each line's tuples, and creating a new PastTrip for each one. Each
	 * of these PastTrips is added to the empty PastTrips. Once the file is fully traversed, PastTrips is returned.
	 */
	public static PastTrips parse(String dirPath) throws FileNotFoundException {
		// Get instance/any previous instance of pastTrips object
		PastTrips pastTrips = PastTrips.getInstance();

		// List of files within provided directory path
		File folder = new File(dirPath);
		File[] listOfFiles = folder.listFiles();
		if (listOfFiles.length == 0)
			throw new FileNotFoundException("Empty folder.");
		for (File file : listOfFiles) {
			if (file.isFile()) {
				try {
//							if (file == listOfFiles[0])
//								System.out.print("Importing ");
//							System.out.print(file.getName() + (file == listOfFiles[listOfFiles.length - 1] ? ". " : ", "));
					Scanner input = new Scanner(file);
					if (input.hasNextLine())
						input.nextLine();

					// Read each line of the file and split by comma regex
					while (input.hasNext()) {
						String trip = input.nextLine();
						String[] tempArray = trip.split(",");

						// Local variables to store attributes of each trip
						int startStation = Integer.parseInt(tempArray[1]);
						int endStation = Integer.parseInt(tempArray[3]);
						int duration = Integer.parseInt(tempArray[4]);

						// Add a past trip to past trips object
						pastTrips.addTrip(new PastTrip(startStation, endStation, duration));

					}
					input.close();
				}

				// Catch block
				catch (FileNotFoundException e) {
					throw e;
				}
			}
		}

		return pastTrips;
	}
}
