//Main code package for BixiTriph
package bixiTrip;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.SystemColor;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

/**
 * @file BixiTripUI.java
 * @author Alan Scott
 * @brief Main driver class for BixiTrip
 * 
 * This class creates a new JFrame and adds the necessary GUI elements onto it. Acts as the main
 * interface between the user and the program since it is a subclass of JFrame,
 */
public class BixiTripUI extends JFrame {

	/** JPanel contain the GUI elements */
	private JPanel contentPane; 
	/** Status field to notify the user of the state of the program */
	private JTextField statusField = new JTextField(); 
	/** Progress bar to show the user the progress of the parsing */
	private JProgressBar progressBar = new JProgressBar(); 
	/** Button that receives user input for the generation of directions */
	private JButton directionsButton = new JButton("Get Directions"); 
	private static Color bixiRed = new Color(213, 43, 30), error = new Color(255, 0, 0);
	/** List of all of the parsed stations */
	private ArrayList<Station> stationsList; 
	/** Button to receive user input to swap the values of the combo box */
	private JButton swapButton = new JButton(); 
	/** Button to receieve user input to open the repository */
	private JButton inspectButton = new JButton(); 
	/** Button that receive user input to open the test GUI */
	private JButton testButton = new JButton(); 
	private final Font smallTextFont16 = new Font("Tahoma", Font.PLAIN, 16);
	private final Font smallTextFont14 = new Font("Tahoma", Font.PLAIN, 14);
	private final Font labelFont = new Font("Tahoma", Font.PLAIN, 24);
	/** Combo box that receives user's start station selection */
	private JComboBox startStationComboBox; 
	/** Combo box that receives the user's end station selection */
	private JComboBox endStationComboBox; 
	/** Primary colour scheme colour */
	private final Color primaryBackgroudColour = new Color(255,255,255);

	/**
	 * @brief Main runnner method for BixiTrip
	 * @param args Arguments passed from command line
	 * 
	 * Creates a new BixiTripUI and begins the parsing process. Catches any exceptions with a try/catch statement
	 */
	public static void main(String[] args) {
		javax.swing.UIManager.put("ProgressBar.selectionBackground", Color.black);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BixiTripUI frame = new BixiTripUI();
					frame.setVisible(true);
					frame.parse();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @brief Initialize the main frame and populate it with GUI elements
	 * 
	 * This method creates a new BixiTripUI object, which itself is a JFrame object. 
	 * Each GUI element is created, placed on the main frame, and enabled. The populate() method is invoked
	 * by this method to populate the data sets and combo boxes. 
	 */
	public BixiTripUI() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage("icons\\runningIcon.png"));

		setTitle("BixiTrip");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 799, 400);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.menu);
		contentPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("icons\\logo2.png"));
		lblNewLabel.setForeground(new Color(255, 0, 0));
		lblNewLabel.setBounds(7, 7, 800, 38);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 32));
		contentPane.add(lblNewLabel);

		startStationComboBox = new JComboBox();
		startStationComboBox.setEnabled(false);
		startStationComboBox.setFont(smallTextFont16);
		startStationComboBox.setBounds(176, 53, 493, 38);
		contentPane.add(startStationComboBox);

		JLabel lblStartStation = new JLabel("Start Station:");
		lblStartStation.setFont(labelFont);
		lblStartStation.setForeground(bixiRed);
		lblStartStation.setBounds(22, 54, 142, 37);
		contentPane.add(lblStartStation);

		JLabel lblEndStation = new JLabel("End Station:");
		lblEndStation.setForeground(bixiRed);
		lblEndStation.setFont(labelFont);
		lblEndStation.setBounds(22, 115, 142, 37);
		contentPane.add(lblEndStation);
		
		JLabel devTools = new JLabel("Developer Tools:");
		devTools.setFont(labelFont);
		devTools.setForeground(bixiRed);
		devTools.setBounds(22, 307, 300, 37);
		contentPane.add(devTools);
		
		JLabel inspectLabel = new JLabel("Code & Documentation");
		inspectLabel.setFont(smallTextFont16);
		inspectLabel.setForeground(bixiRed);
		inspectLabel.setBounds(280, 307, 300, 37);
		contentPane.add(inspectLabel);
		
		JLabel testLabel = new JLabel("Validation");
		testLabel.setFont(smallTextFont16);
		testLabel.setForeground(bixiRed);
		testLabel.setBounds(560, 307, 300, 37);
		contentPane.add(testLabel);

		endStationComboBox = new JComboBox();
		endStationComboBox.setEnabled(false);
		endStationComboBox.setFont(smallTextFont16);
		endStationComboBox.setBounds(176, 115, 493, 38);
		contentPane.add(endStationComboBox);
		
		testButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonHandler.test();
			}
		});
		testButton.setEnabled(true);
		//setIcon
		testButton.setBounds(500,300,50,50);
		testButton.setToolTipText("Run Tests");
		testButton.setIcon(new ImageIcon("icons\\clipboard.png"));
		testButton.setEnabled(false);
		contentPane.add(testButton);
		
		swapButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonHandler.swap(startStationComboBox, endStationComboBox);
			}
		});
		
		swapButton.setEnabled(true);
		//swapButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		swapButton.setIcon(new ImageIcon("icons\\arrows.png"));
		swapButton.setBounds(690, 65, 75, 75);
		swapButton.setToolTipText("Swap Start and End");
		contentPane.add(swapButton);
		
		inspectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonHandler.inspect();
			}
		});
		inspectButton.setEnabled(true);
		//inspectButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		inspectButton.setIcon(new ImageIcon("icons\\link.png"));
		inspectButton.setBounds(220, 300, 50, 50);
		inspectButton.setToolTipText("Inspect Code & Documentation");
		contentPane.add(inspectButton);
		

		// JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(12, 217, 757, 14);
		progressBar.setForeground(bixiRed);
		contentPane.add(progressBar);

		// JButton directionsButton = new JButton("Get Directions");
		directionsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DirectionHandler.run(statusField, startStationComboBox, endStationComboBox, stationsList);
			}
		});
		
		directionsButton.setEnabled(false);
		directionsButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		directionsButton.setBounds(309, 166, 200, 38);
		contentPane.add(directionsButton);

		// statusField = new JTextField();
		statusField.setEditable(false);
		statusField.setHorizontalAlignment(SwingConstants.CENTER);
		statusField.setFont(smallTextFont16);
		statusField.setBounds(12, 244, 757, 38);
		contentPane.add(statusField);
		statusField.setColumns(10);
		
		//colour settings
		contentPane.setBackground(primaryBackgroudColour);
		statusField.setBackground(primaryBackgroudColour);
		testButton.setBackground(primaryBackgroudColour);
		inspectButton.setBackground(primaryBackgroudColour);
		swapButton.setBackground(primaryBackgroudColour);
		directionsButton.setBackground(primaryBackgroudColour);
		startStationComboBox.setBackground(primaryBackgroudColour);
		endStationComboBox.setBackground(primaryBackgroudColour);
		
		populate();
		
		startStationComboBox.setEnabled(true);
		endStationComboBox.setEnabled(true);
	}
	
	/**
	 * @brief Method to gather data from the files
	 * 
	 * This method gets data from the .csv file and populates the stations using the
	 * relevant parsing function. This parsed data is then added to the combo boxes. 
	 */
	private void populate() {
		String stationPath = "stations\\Stations_2018.csv", pastTripsPath = "pastTrips";
		File stationFile;

		// initialize PastTrips and Stations abstract objects
		Stations stations = new Stations();
		PastTrips pastTrips = new PastTrips();

		stationFile = new File(stationPath);
		if (!stationFile.exists()) {
			statusField.setForeground(error);
			statusField.setText("Stations file could not be found. Please correct the file path and try again.");
			return;
		}
		try {
			stations = StationsParser.parse(stationPath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		statusField.setText("Welcome to BixiTrip! Please wait while we import our data.");

		// populate combobox
		stationsList = algs.StationsMergesort.sortByName(stations.getStations());

		startStationComboBox.addItem("");
		endStationComboBox.addItem("");
		for (Station element : stationsList) {
			startStationComboBox.addItem(element.getName());
			endStationComboBox.addItem(element.getName());
		}
	}
	
	/**
	 * @brief This method creates a concurrent worker to parse past trips.
	 * 
	 * This method creates a concurrent SwingWorker from the ParseWorker class. It then executes
	 * the worker to work in the background.
	 */
	public void parse() {
		SwingWorker<Boolean,String> worker = new ParseWorker(progressBar, statusField, directionsButton, testButton);
		worker.execute();
	}
}
