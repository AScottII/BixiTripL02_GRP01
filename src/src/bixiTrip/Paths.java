package bixiTrip;

import java.util.ArrayList;

import algs.Graph;

/**
 * @file Paths.java
 * @brief Aggregate class for Path objects
 * @author Alan Scott
 */

public class Paths {
	
	/** Node-edge graph representation of all of the paths */
	private final Graph graph;
	/** Collection of all the stations in the network */
	private final Stations stations;
	/** Initial singleton instance of the class, initially null */
	private static Paths instance = null;

	/**
	 * @brief Get method for a singleton Paths object
	 * @return Returns a single instance of the Paths object, creates a new one if one does not exist
	 */
	public static Paths getInstance() {
		if (instance == null)
			instance = new Paths();
		return instance;
	}

	/**
	 * @brief Constructor for the Paths object
	 */
	private Paths() {
		stations = Stations.getInstance();
		this.graph = new Graph();
	}

	/**
	 * @brief Add a Path to the Paths object
	 * @param path Path to add to the object
	 */
	public void addPath(Path path) {
		graph.addPath(path);
	}

	/**
	 * @brief Get a Path that's stored in the Paths object
	 * @param startCode Code of the Station where the Path starts
	 * @param endCode   Code of the Station where the Path end
	 * @return Requested Path if it exists in Paths or null if it does not
	 */
	public Path getPath(int startCode, int endCode) {
		return graph.getPath(startCode, endCode);
	}

	/**
	 * @brief Import all PastTrip's into the Paths object
	 * 
	 * This method first gets an instance of PastTrips. It iterates through each past trip in
	 * PastTrips, creating a new Path object for each. Each of these paths is added into the Paths object.
	 */
	public void importPastTrips() {
		PastTrips pastTrips = PastTrips.getInstance();

		int i = 0;
		ArrayList<PastTrip> nextPath;
		while (true) {

			nextPath = pastTrips.getNextPath();
			if (nextPath == null)
				return;

			for (PastTrip pastTrip : nextPath) {
				Path path = getPath(pastTrip.getStartCode(), pastTrip.getEndCode());

				if (path == null)
					addPath(new Path(pastTrip));
				else
					path.addPastTrip(pastTrip);
			}
			i++;
		}
	}

	/**
	 * @brief Get method for Graph containing all Paths
	 * @return Returns all Paths connected in a Graph object
	 */
	public Graph getGraph() {
		return graph;
	}
}
