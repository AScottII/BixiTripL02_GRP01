package bixiTrip;

/**
 * @file PastTrip.java
 * @author Alan Scott
 * @brief Abstract data type representing a past trip taken by a commuter
 */
public class PastTrip implements Comparable<PastTrip> {
	
	/** code representing the start station of this past trip */
	protected final int startCode;
	/** code representing the end station of this past trip */
	protected final int endCode;
	/** code representing the duration of this past trip */
	private final int duration;

	/**
	 * @brief PastTrip constructor method
	 * @param _startCode Station start code
	 * @param _endCode   Station end code
	 * @param _duration  Duration of the trip
	 * 
	 * This method takes in a start code, end code, and duration and sets these values as
	 * constants in the PastTrip object
	 */
	public PastTrip(int _startCode, int _endCode, int _duration) {
		this.startCode = _startCode;
		this.endCode = _endCode;
		this.duration = _duration;
	}

	/**
	 * @brief Function that returns the starting station
	 * @return The start station
	 */
	public int getStartCode() {
		return this.startCode;
	}

	/**
	 * @brief Function that returns the ending station
	 * @return The end station
	 */
	public int getEndCode() {
		return this.endCode;
	}

	/**
	 * @brief Function that return the duration of the trip
	 * @return The duration of the trip
	 */
	public int getDuration() {
		return this.duration;
	}

	@Override
	/**
	 * @brief PastTrip compare method
	 * @param trip The trip being compared to
	 * @return Integer representing the equivalence of the two trips (1 if same, 0 if not)
	 * 
	 * This method compares two trips by matching their start and end codes
	 */
	public int compareTo(PastTrip trip) {
		int start = Integer.compare(this.getStartCode(), trip.getStartCode());
		if (start == 0) {
			return Integer.compare(this.getEndCode(), trip.getEndCode());
		} else {
			return start;
		}
	}
}
