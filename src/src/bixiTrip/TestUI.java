package bixiTrip;

import javax.swing.*;
import tests.CompiledTests;
import java.awt.Dimension;

/**
 * @file TestUI.java
 * @author Alan Scott
 * @brief This class provides a graphical report of all of the tests run on the program. This class interfaces with the user as a JFrame
 */
public class TestUI extends JFrame{
	
	/** JPanel containing GUI elements */
	JPanel JP;
	/** JLabel providing String output to the user */
	JLabel JL;
	
	/**
	 * @brief TestUI constructor/runner method
	 * 
	 * This constructor creates a new JFrame and JLabel to display the outputs of the front end testing.
	 * The output JLabel appends the outputs of each test to itself, displaying on the JFrame when all tests are
	 * complete. 
	 */
	public TestUI() {
		JP = new JPanel();
		setContentPane(JP);
		JL = new JLabel();
		JL.setPreferredSize(new Dimension(500,500));
		JL.setVerticalAlignment(JLabel.TOP);
		JL.setHorizontalAlignment(JLabel.LEFT);
		JL.setText("Running tests...");
		JP.add(JL);
		setResizable(false);
		setTitle("Validation");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(600,250);
		setVisible(true);
		
		String outlabel = "<html> <br> <br>";
		CompiledTests T = new CompiledTests();

		outlabel = append(outlabel, T.testCoord());
		outlabel = append(outlabel, T.testStation());
		outlabel = append(outlabel, T.testStations());
		outlabel = append(outlabel,T.testPastTrip());
		outlabel = append(outlabel,T.testPastTrips());
		outlabel = append(outlabel, T.testPath());
		
		outlabel += "</html>";
		
		JL.setText(outlabel);
	}
	
	/**
	 * @brief This method takes in the inputs from the test method and formats them
	 * @param outlabel Current aggregated label from the constructor
	 * @param temp Temporary string containing output arguments
	 * @return The formatted string label
	 */
	private String append(String outlabel, String[] temp) {
		outlabel += temp[0] + " " + temp[1] + " with " + temp[2] + " test cases passing<br>";
		return outlabel;
	}
	
	public static void main(String[] args) {
		new TestUI();
	}
		
}
