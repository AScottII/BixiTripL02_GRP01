package bixiTrip;

/**
 * @file Coord.java
 * @brief Coordinate abstract data type
 * @author Alan Scott
 */
public class Coord {
	/** Longitude value of the coordinate */
	private final double longitude;
	/** Latitude value of the coordinate */
	private final double latitude;

	/**
	 * @brief Coord constructor method
	 * @param _lat Input latitude
	 * @param _long Input longitute
	 * 
	 * This method sets the final values of the latitude and longitude for the Coord object
	 */
	public Coord(double _lat, double _long) {
		this.longitude = _long;
		this.latitude = _lat;
	}

	/**
	 * @brief Get method for the Longitude.
	 * @return Longitude of coordinate
	 */
	public double getLong() {
		return longitude;
	}

	/**
	 * @brief Get method for the Latitude.
	 * @return Latitude of coordinate
	 */
	public double getLat() {
		return latitude;
	}

	@Override
	/**
	 * @brief Converts coordinate to string
	 * @return String representation of coordinate
	 */
	public String toString() {
		return getLat() + "," + getLong();
	}
}
