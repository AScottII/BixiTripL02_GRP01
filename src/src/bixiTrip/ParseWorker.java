package bixiTrip;

import java.awt.Color;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 * @file ParseWorker.java
 * @author Alan Scott
 * @brief Concurrent worker class to parse past trips
 */
public class ParseWorker extends SwingWorker<Boolean,String> {

	/** Collection of past trips */
	String pastTripsPath;
	/** .csv file containing all past trips */
	File pastTripsDir;
	/** Empty instance of the pastTrips class */
	PastTrips pastTrips = PastTrips.getInstance();
	/** Empty instance of the Paths class */
	Paths paths = Paths.getInstance();
	/** Passed in status field, will be updated by the parse to inform user of the progress */
	JTextField statusField;
	/** Passed in progress bar, will be updated by the parse to inform user of the progress */
	JProgressBar progressBar;
	private static Color bixiRed = new Color(213, 43, 30), error = new Color(255, 0, 0);
	/** Passed in directions button, will be enabled once parsing is done */
	JButton directionsButton;
	/** Passed in testing button, will be enabled once parsing is done */
	JButton testButton;
	
	/**
	 * @brief ParseWorker constructor method
	 * @param progressBar ProgressBar from BixiTripUI
	 * @param statusField JTextField from BixiTripUI
	 * @param directionsButton JButton from BixiTripUI
	 * 
	 * The constructor takes input from the BixiTripUI for the purpose of displaying output. It
	 * also receives the most current version of the static classes pastTrip and Paths.
	 */
	public ParseWorker(JProgressBar progressBar, JTextField statusField, JButton directionsButton, JButton testButton) {
		pastTrips = pastTrips.getInstance();
		paths = Paths.getInstance();
		this.progressBar = progressBar;
		this.statusField = statusField;
		this.directionsButton = directionsButton;
		this.testButton = testButton;
	}
	
	@Override
	/**
	 * @brief PastTrip parsing method
	 * @exception Exception Exception is thrown if the file containing the past trips cannot be found or there is a parsing error
	 * @return true if operation is successful, false if an error is encountered
	 * 
	 * This method parses all of the past trips contained within the past trips .csv file, throwing an
	 * error if it cannot be found. It makes use of the PastTripsParser to do so, and uses the Paths class to generate
	 * paths for every past trip. Throughout the whole process, the method makes use of the passed GUI elements
	 * to update the user on the progress of the operation. At the very end, the directions button is enable, such
	 * that the user may begin to use the program. 
	 */
	protected Boolean doInBackground() throws Exception {

		// import pastTrips and add them to object
		pastTripsPath = "pastTrips";
		pastTripsDir = new File(pastTripsPath);
		progressBar.setIndeterminate(true);
		progressBar.setStringPainted(true);
		try {
			progressBar.setString("Parsing past trips...");
			pastTrips = PastTripParser.parse(pastTripsPath);
		} catch (Exception f) {
			progressBar.setStringPainted(false);
			progressBar.setIndeterminate(false);
			statusField.setForeground(error);
			statusField.setText(
					"Past trips directory couldn't be found. Please correct the file path and try again.");
			return false;
		}

		// calculate paths
		try {
			progressBar.setString("Creating paths...");
			paths.importPastTrips();
		} catch (Exception e) {
			progressBar.setStringPainted(false);
			progressBar.setIndeterminate(false);
			statusField.setForeground(error);
			statusField.setText("An error occurred during the path conversion. Please try again.");
			return false;
		}

		// update UI to show completion
		progressBar.setIndeterminate(false);
		progressBar.setString("");
		progressBar.setStringPainted(false);
		directionsButton.setEnabled(true);
		testButton.setEnabled(true);
		statusField.setForeground(new Color(0, 0, 0));
		statusField.setText("Imported successfully. Directions enabled.");
		return true;
	}
}
