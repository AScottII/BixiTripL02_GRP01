package bixiTrip;

/**
 * @file Station.java
 * @brief A class representing the Station abstract data type.
 * @author Alan Scott
 *
 */

public class Station implements Comparable<Station> {
	/** Station code */
	private final Integer code;
	/** Name of the station */
	private final String name;
	/** Lat/Long coordinates of the station */
	private final Coord coords;

	/**
	 * @brief Constructor for a new Station object.
	 * @param _code   Integer representing the station code.
	 * @param _name   String representing the station's name.
	 * @param _coords Coord representing the coordinates of the station.
	 * 
	 * This constructor creates a new Station and assigns the state variables constant values
	 */
	public Station(int _code, String _name, Coord _coords) {
		this.code = _code;
		this.name = _name;
		this.coords = _coords;
	}

	/**
	 * @brief Get method for the station's code.
	 * @return Returns station's code as an integer.
	 */
	public int getCode() {
		return this.code;
	}

	/**
	 * @brief Get method for the name of the station.
	 * @return Returns station's name as a string.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @brief Get method for the coordinates of the station.
	 * @return Returns station's coordinates as type Coord.
	 */
	public Coord getCoords() {
		return this.coords;
	}

	@Override
	/**
	 * @brief A function to implement the Comparable interface to the Station data type.
	 * @param that A Station object to be compared to the local object.
	 * @return Returns an integer 1 if the object's code is greater than that, -1 if
	 *         it is less and 0 if they are the same.
	 */
	public int compareTo(Station that) {
		return (this.code).compareTo(that.getCode());
	}

}
